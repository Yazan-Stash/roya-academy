<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsletterSubscriber;
use App\Mail\SubscribedToNewsletter;
use Illuminate\Support\Facades\Mail;

class NewsletterSubscribersController extends Controller
{
    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:newsletter_subscribers'
        ]);

    	$subscriber = NewsletterSubscriber::create([
    		'email' => $request->email,
    		'activation_code' => str_random('10')
    	]);

    	Mail::to($subscriber->email)->queue(new SubscribedToNewsletter($subscriber));

    	return response()->json(['message' => 'تم إرسال طلبك، يرجى مراجعة بريدك الإلكتروني']);
    }

    public function activateEmail($code)
    {
    	$subscriber = NewsletterSubscriber::where('activation_code', $code)->first();

    	if (! $subscriber) {
    		abort(404);
    	}

    	$subscriber->activation_code = null;
    	$subscriber->save();

    	return view('newsletter.activated')
    		->with('email', $subscriber->email);
    }
}
