<?php

namespace App\Http\Controllers;

use App\Staff;

class AboutPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('about')->with('staff', Staff::get());
    }
}