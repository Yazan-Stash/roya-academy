<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Support\Facades\DB;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('courses.index')
            ->with('courses', Course::latest()->paginate(12));
    }

    public function show($id)
    {
        return view('courses.show')
            ->with('course', Course::with('instructors')->findOrFail($id));
    }

    public function intro()
    {
        return view('courses.introduction')
            ->with('intro', DB::table('courses_introduction')->first());
    }

    public function diploma()
    {
        return view('courses.diploma')
            ->with('diploma', DB::table('courses_diploma')->first());
    }
}
