<?php

namespace App\Http\Controllers;

use App\Post;
use App\Staff;
use App\Course;
use App\Slider;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index')
            ->with('sliders', Slider::limit(10)->get())
            ->with('courses', Course::latest()->limit(5)->get())
            ->with('staff', Staff::limit(5)->get())
            ->with('posts', Post::published()->latest()->limit(4)->get());
    }
}
