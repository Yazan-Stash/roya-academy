<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagesController extends Controller
{
    /**
         * Upload images on tmp folder and return url or full path
         *
         * @param string $returnType
         * @return $this
         */
        public function uploadTemp($returnType = 'url')
        {
            $image_name = date("Ymd");
            $path = request()->file('file')->store('public/images/tmp/' . $image_name);

            if ($returnType == 'url')
                $message = '/storage/images/tmp/' . $image_name . '/' . basename($path);
            if ($returnType == 'path')
                $message = $path;

            $response = response()->json($message ?? null)
                ->header('Content-Type', 'application/json')
                ->header('charset', 'utf-8');

            return $response;
        }
}
