<?php

namespace App\Http\Controllers\AdminControllers;

use App\Gallery;
use App\Jobs\OptimizeImage;
use Facades\App\ImageStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.gallery.index')->with('gallery', Gallery::orderBy('created_at', 'desc')->paginate(10));
    }

    public function create()
    {
        return view('admin.gallery.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'type' => 'required',
            'video' => 'nullable|required_if:type,video|mimes:mp4',
            'image' => 'nullable|required_if:type,image',
            'youtube_url' => 'nullable|required_if:type,youtube|string',
        ]);

        if (! empty($request->youtube_url)) {
            if ( !str_contains(request()->youtube_url, 'embed')) {
                Session::flash('error', 'يرجى نسخ رابط embed بدل من رابط الفيديو');
                return redirect()->back()->withInput(
                    request()->only('title', 'youtube_url')
                );
            }
            $this->saveAsYoutube();        
        } elseif (! empty($request->video)) {
            $this->saveAsVideo();
        } elseif (! empty($request->image)) {
            $this->saveAsImage();
        } else {
            Session::flash('error', 'يرجى رفع عنصر واحد على الاقل');
            return redirect()->back();
        }

        return redirect()->route('admin.gallery.index');
    }

    public function edit($id)
    {
        $gallery = Gallery::find($id);
        return view('admin.gallery.edit',compact('gallery'));
    }

    public function update(Request $request, Gallery $gallery)
    {
        $gallery->title = $request->title;

        if ($request->filled('youtube_url')) {
            $gallery->youtube_url = $request->youtube_url;
        }
        
        $gallery->save();

        return redirect()->route('admin.gallery.index');
    }

    public function destroy(Gallery $gallery)
    {
        if ($gallery->isNotYoutube()) {
            Storage::disk('gallery')->delete($gallery->path);
        }

        $gallery->delete();
        
        return redirect()->route('admin.gallery.index');
    }

    public function saveAsYoutube()
    {
        return Gallery::create(['title' => request()->title, 'youtube_url' => request()->youtube_url]);
    }

    public function saveAsVideo()
    {
        return  Gallery::create(['title' => request()->title, 'path' => request()->file('video')->store('videos', 'gallery')]);
    }

    public function saveAsImage()
    {
        $gallery = Gallery::create(['title' => request()->title, 'path' => ImageStore::moveRequestImageTo('gallery')]);
    
        OptimizeImage::dispatch($gallery->path);

        return $gallery;
    }   
}
