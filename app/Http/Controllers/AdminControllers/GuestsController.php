<?php

namespace App\Http\Controllers\AdminControllers;

use App\Guest;
use App\Jobs\OptimizeImage;
use Facades\App\ImageStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class GuestsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.guests.index')
            ->with('guests', Guest::paginate(10));
    }

    public function create()
    {
        return view('admin.guests.add');
    }

    public function store(Request $request)
    {
        $validated_inputs = $this->validate($request, [
            'name' => 'required|max:55',
            'image' => 'required',
            'bio' => 'nullable'
        ]);

        $path = ImageStore::moveRequestImageTo('guests');

        $guest = Guest::create(array_merge($validated_inputs, ['image' => $path]));

        OptimizeImage::dispatch($guest->image);

        return redirect()->route('admin.guests.index');
    }

    public function edit(Guest $guest)
    {
        return view('admin.guests.edit')
            ->with('guest', $guest);
    }

    public function update(Request $request, Guest $guest)
    {
        $validated_inputs = $this->validate($request, [
            'name' => 'required|max:55',
            'bio' => 'nullable'
        ]);

        if (! empty($request->image)) {
            ImageStore::delete($guest->image);
            $image = ['image' => ImageStore::moveRequestImageTo('guests')];

            OptimizeImage::dispatch($image['image']);
        } else {
            $image = [];
        }

        $guest->fill(
            array_merge(
                $validated_inputs, 
                $image
            )
        )->save();

        return redirect()->route('admin.guests.index');
    }

    public function destroy(Guest $guest)
    {
        Storage::disk('images')->delete($guest->image);
        $guest->delete();

        return redirect()->route('admin.guests.index');
    }
}
