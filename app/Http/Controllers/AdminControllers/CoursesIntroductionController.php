<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CoursesIntroductionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }
    
    public function edit()
    {
        return view('admin.courses.introduction.edit')
            ->with('intro', DB::table('courses_introduction')->first());
    }

    public function update(Request $request)
    {
        $result = DB::table('courses_introduction')
            ->where('id', '1')
            ->update(['intro' => $request->intro]);

        Session::flash('success', 'تم تعديل وصف الدورات');
        return redirect()->route('admin.courses.index');
    }
}
