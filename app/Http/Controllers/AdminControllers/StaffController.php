<?php

namespace App\Http\Controllers\AdminControllers;

use App\Staff;
use App\Jobs\OptimizeImage;
use Facades\App\ImageStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.staff.index')
            ->with('staff', Staff::latest()->paginate(10));
    }

    public function create()
    {
        return view('admin.staff.add');
    }

    public function store(Request $request)
    {
        $validated_inputs = $this->validate($request, [
            'name' => 'required|max:55',
            'title' => 'required|max:55',
            'bio' => 'required',
            'image' => 'required',
            'email' => 'nullable|email',
            'phone' => 'nullable'
        ]);
        
        $path = ImageStore::moveRequestImageTo('staff');

        $staff = Staff::create(array_merge($validated_inputs, ['image' => $path]));
        
        OptimizeImage::dispatch($staff->image);

        return redirect()->route('admin.staff.index');
    }

    public function edit($id)
    {
        $staff = Staff::find($id);
        return view('admin.staff.edit',compact('staff'));
    }

    public function update(Request $request, Staff $staff)
    {
        $validated_inputs = $this->validate($request, [
            'name' => 'required|max:55',
            'title' => 'required|max:55',
            'bio' => 'required',
            'email' => 'nullable|email',
            'phone' => 'nullable'
        ]);

        if (! empty($request->image)) {
            ImageStore::delete($staff->image);
            $image = ['image' => ImageStore::moveRequestImageTo('staff')];

            OptimizeImage::dispatch($image['image']);
        } else {
            $image = [];
        }

        $staff->fill(
            array_merge(
                $validated_inputs, 
                $image
            )
        )->save();

        return redirect()->route('admin.staff.index');
    }

    public function destroy(Staff $staff)
    {
        Storage::disk('images')->delete($staff->image);
        $staff->delete();

        return redirect()->route('admin.staff.index');
    }
}
