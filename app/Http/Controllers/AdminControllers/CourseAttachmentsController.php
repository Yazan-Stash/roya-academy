<?php

namespace App\Http\Controllers\AdminControllers;

use App\Course;
use App\Attachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CourseAttachmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }
    
    public function index(Course $course)
    {
        return view('admin.courses.attachments.index')
            ->with('course', $course)
            ->with('attachments', $course->attachments);
    }

    public function store(Request $request, Course $course)
    {
        $validated_inputs = $this->validate($request, [
            'title' => 'required|string',
            'attachment' => 'required|mimes:pdf|file',
        ]);

        $course->attachments()->create([
            'title' => $request->title,
            'path' => $request->file('attachment')->store('', 'attachments')
        ]);

        return redirect()->route('admin.courses.attachments.index', ['course' => $course->id]);
    }

    public function destroy(Course $course, Attachment $attachment)
    {
        Storage::delete($attachment->path);

        $attachment->delete();

        return redirect()->route('admin.courses.attachments.index', ['course' => $course->id]);
    }
}
