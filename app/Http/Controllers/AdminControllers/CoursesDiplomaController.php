<?php

namespace App\Http\Controllers\AdminControllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CoursesDiplomaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }
    
    public function edit()
    {
        return view('admin.courses.diploma.edit')
            ->with('diploma', DB::table('courses_diploma')->first());
    }

    public function update(Request $request)
    {
        $result = DB::table('courses_diploma')
            ->where('id', '1')
            ->update(['diploma' => $request->diploma]);

        Session::flash('success', 'تم تعديل وصف برامج الدبلوم');
        return redirect()->route('admin.courses.index');
    }
}
