<?php

namespace App\Http\Controllers\AdminControllers;

use App\Guest;
use App\Staff;
use App\Course;
use App\Jobs\OptimizeImage;
use Facades\App\ImageStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.courses.index')->with('courses', Course::orderBy('created_at', 'desc')->paginate(10));
    }

    public function create()
    {
        return view('admin.courses.add')
            ->with('guests', Guest::all())
            ->with('instructors', Staff::all());
    }

    public function store(Request $request)
    {
        $validated_inputs = $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'category' => 'required|max:55',
            'image' => 'required',
            'status' => 'required|in:انتهت,قريباً,حالياً',
            'price' => 'nullable|numeric',
            'duration' => 'nullable|string',
            'date' => 'nullable|string',
            'seats' => 'nullable|numeric',
        ]);

        $validated_inputs['image'] = ImageStore::moveRequestImageTo('courses');

        $course = Course::create(array_merge(
            $validated_inputs,
            ['description' => str_replace('../../storage/images/inner', config('env.image_cdn') . 'inner', $validated_inputs['description'])]
        ));
        $course->instructors()->sync($request->instructors);
        $course->guests()->sync($request->guests);

        OptimizeImage::dispatch($course->image);

        return redirect()->route('admin.courses.index');
    }

    public function edit($id)
    {
        $instructors = Staff::all();
        $guests = Guest::all();
        $course = Course::with(['instructors', 'guests'])->find($id);
        $original_instructors = $course->instructors->pluck('id')->toArray();
        $original_guests = $course->guests->pluck('id')->toArray();
        
        return view('admin.courses.edit',compact('course', 'instructors', 'guests', 'original_instructors', 'original_guests'));
    }

    public function update(Request $request, Course $course)
    {
        $validated_inputs = $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'category' => 'required|max:55',
            'status' => 'required|in:انتهت,قريباً,حالياً',
            'price' => 'nullable|numeric',
            'duration' => 'nullable|string',
            'date' => 'nullable|string',
            'seats' => 'nullable|numeric',
        ]);
        
        if (! empty($request->image)) {
            ImageStore::delete($course->image);
            $image = ['image' => ImageStore::moveRequestImageTo('courses')];

            OptimizeImage::dispatch($image['image']);
        } else {
            $image = [];
        }

        $course->fill(array_merge(
            $validated_inputs, 
            $image,
            ['description' => str_replace('../../../storage/images/inner', config('env.image_cdn') . 'inner', $validated_inputs['description'])]
        ))->save();

        $course->instructors()->sync($request->instructors);
        $course->guests()->sync($request->guests);

        return redirect()->route('admin.courses.index');
    }

    public function destroy(Course $course)
    {
        Storage::disk('images')->delete($course->image);

        $course->delete();
        
        return redirect()->route('admin.courses.index');
    }
}
