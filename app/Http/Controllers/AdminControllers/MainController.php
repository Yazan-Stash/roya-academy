<?php

namespace App\Http\Controllers\AdminControllers;

use App\Post;
use App\Course;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.index')->with('posts', Post::orderBy('created_at', 'desc')->limit(6)->get())
                                  ->with('courses', Course::orderBy('created_at', 'desc')->limit(6)->get());
    }
}
