<?php

namespace App\Http\Controllers\AdminControllers;

use App\Post;
use App\Jobs\OptimizeImage;
use Facades\App\ImageStore;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.posts.index')->with('posts', Post::orderBy('created_at', 'desc')->paginate(10));
    }

    public function create()
    {
        return view('admin.posts.add');
    }

    public function store(Request $request)
    {
        $validated_inputs = $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
            'published' => 'boolean',
            'image' => 'required',
        ]);
        
        $post = Post::create(array_merge(
            $validated_inputs, 
            [
                'image' => ImageStore::moveRequestImageTo('posts'),
                'slug' => str_slug($request->title),
            ]
        ));

        OptimizeImage::dispatch($post->image);

        return redirect()->route('admin.posts.index');
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('admin.posts.edit',compact('post'));
    }

    public function update(Request $request, Post $post)
    {
        $validated_inputs = $this->validate($request, [
            'title' => 'required|max:255',
            'body' => 'required',
            'published' => 'boolean',
        ]);

        if (! empty($request->image)) {
            ImageStore::delete($post->image);
            $image = ['image' => ImageStore::moveRequestImageTo('posts')];

            OptimizeImage::dispatch($image['image']);
        } else {
            $image = [];
        }

        $post->fill(
            array_merge(
                $validated_inputs, 
                $image
            )
        )->save();

        return redirect()->route('admin.posts.index');
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('admin.posts.index');
    }
}
