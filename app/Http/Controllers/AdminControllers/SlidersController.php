<?php

namespace App\Http\Controllers\AdminControllers;

use App\Slider;
use App\Jobs\OptimizeImage;
use Facades\App\ImageStore;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class SlidersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->except('logout');
    }

    public function index()
    {
        return view('admin.sliders.index')->with('slider', Slider::orderBy('created_at', 'desc')->paginate(10));
    }

    public function create()
    {
        return view('admin.sliders.add');
    }

    public function store(Request $request)
    {
        $validated_inputs = $this->validate($request, [
            'heading' => 'nullable|max:55',
            'caption' => 'nullable|max:200',
            'image' => 'required',
            'link' => 'nullable',
        ]);

        if (Slider::count() >= 5) {
            Session::flash('error', 'لا يمكنك إضافة أكثر من خمسة صور لشريط الصور الرئيسي');
            return redirect()->back();
        }

        $path = ImageStore::moveRequestImageTo('sliders');

        $slider = Slider::create(array_merge(
            $validated_inputs,
            ['image' => $path]
        ));

        OptimizeImage::dispatch($slider->image);

        return redirect()->route('admin.sliders.index');
    }

    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.sliders.edit',compact('slider'));
    }

    public function update(Request $request, Slider $slider)
    {
        $validated_inputs = $this->validate($request, [
            'heading' => 'nullable|max:55',
            'caption' => 'nullable|max:200',
            'link' => 'nullable',
        ]);

        if (! empty($request->image)) {
            ImageStore::delete($slider->image);
            $image = ['image' => ImageStore::moveRequestImageTo('sliders')];
            
            OptimizeImage::dispatch($image['image']);
        } else {
            $image = [];
        }

        $slider->fill(
            array_merge(
                $validated_inputs, 
                $image
            )
        )->save();

        return redirect()->route('admin.sliders.index');
    }

    public function destroy(Slider $slider)
    {
        if (Slider::count() <= 2) {
            Session::flash('error', 'لا يمكن ان تكون الصور الرئيسية اقل من 2');
            return redirect()->back();
        }

        Storage::disk('images')->delete($slider->image);
        $slider->delete();

        return redirect()->route('admin.sliders.index');
    }
}
