<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ContactUsController extends Controller
{
	public function create()
	{
		return view('contact');
	}

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required|max:255',
    		'email' => 'required|email',
    		'subject' => 'required|max:255',
    		'message' => 'required',
    	]);
    	
    	Mail::to('academy@roya.tv')->queue(new ContactMessage([
    		'name' => request()->name,
    		'email' => request()->email,
    		'subject' => request()->subject,
    		'message' => request()->message,
    	]));

    	Session::flash('success', 'تم إرسال طلبك بنجاح');
    	return redirect()->back();
    }
}
