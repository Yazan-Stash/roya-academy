<?php

namespace App\Http\Controllers;

use App\Staff;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('staff.index')
            ->with('staff', Staff::paginate(12));
    }

    public function show(Staff $staff)
    {
        return view('staff.show')
            ->with('staff', $staff);
    }
}
