<?php

namespace App\Http\Controllers;

use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('posts.index', [
            'posts' => Post::latest()->published()->paginate(10)
        ]);
    }

    public function show($post)
    {
        $post = Post::published()->findOrFail($post);

        return view('posts.show')->with('post', $post);
    }
}
