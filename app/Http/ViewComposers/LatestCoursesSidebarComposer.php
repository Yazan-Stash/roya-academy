<?php

namespace App\Http\ViewComposers;

use App\Course;
use Illuminate\View\View;

class LatestCoursesSidebarComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('latest_courses', Course::latest()->limit(5)->get());
    }
}
