<?php

namespace App;

use Illuminate\Support\Facades\Storage;

class ImageStore
{
	public function moveRequestImageTo($folder)
	{
		if (empty(request()->image)) {
			abort(422, 'image path is missing from the requrest.');
		}

		$path = "{$folder}/" . basename(request()->image);

		Storage::move(
            request()->image, 
            'public/images/' . $path 
        );

        return $path;
	}

	public function url($image)
	{
		return Storage::disk('images')->url($image);
	}

	public function delete($image)
	{
		return Storage::disk('images')->delete($image);
	}
}
