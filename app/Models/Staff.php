<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Staff extends Model
{
    protected $fillable = ['name', 'title', 'email', 'phone', 'bio', 'image'];

    public function getImageUrlAttribute()
    {
    	return Storage::disk('images')->url($this->image);
    }

    public function excerpt($chars = 70)
    {
        return str_limit(
            html_entity_decode(
                strip_tags($this->bio)
            ), 
            $chars
        );
    }
}
