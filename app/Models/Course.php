<?php

namespace App;

use App\Attachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Course extends Model
{
	protected $guarded = [];
    
    public function instructors()
    {
        return $this->belongsToMany(Staff::class);
    }
    
    public function guests()
    {
        return $this->belongsToMany(Guest::class);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function excerpt($chars = 70)
    {
        return str_limit(
            html_entity_decode(
                strip_tags($this->description)
            ), 
            $chars
        );
    }
    
    public function getImageUrlAttribute()
    {
    	return Storage::disk('images')->url($this->image);
    }
}
