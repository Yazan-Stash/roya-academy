<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Slider extends Model
{
    protected $guarded = [];

    public function getLinkAttribute()
    {
        return $this->attributes['link'] ?? '#';
    }

    public function getImageUrlAttribute()
    {
    	return Storage::disk('images')->url($this->image);
    }

    public function excerpt($chars = 70)
    {
        return str_limit(
            html_entity_decode(
                strip_tags($this->caption)
            ), 
            $chars
        );
    }
}
