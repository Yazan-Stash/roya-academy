<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Gallery extends Model
{
    protected $table = 'gallery';
    protected $guarded = [];

    public function getTypeAttribute()
    {
        if ($this->isYoutube()) {
            return 'youtube';
        } elseif ($this->isVideo()) {
            return 'video';
        }

        return 'image';
    }

    public function isYoutube()
    {
        return (! empty($this->youtube_url) && str_contains($this->youtube_url, 'youtube.com'));
    }

    public function isNotYoutube()
    {
        return (empty($this->youtube_url) && ! empty($this->path));
    }

    public function isImage()
    {
        return str_contains($this->path, ['.jpg', '.jpeg', '.png']);
    }

    public function isVideo()
    {
        return str_contains($this->path, ['.mp4']);
    }

    public function getAssetUrlAttribute()
    {
        if ($this->isImage()) {
            return Storage::disk('images')->url($this->path);
        } elseif ($this->isVideo()) {
            return Storage::disk('gallery')->url($this->path);
        }
    }

    public function scopeImagesOnly($query)
    {
        return $query
            ->where('path', 'like', '%.jpg%')
            ->orWhere('path', 'like', '%.jpeg%')
            ->orWhere('path', 'like', '%.png%');
    }
}
