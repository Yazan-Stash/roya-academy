<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Guest extends Model
{
    protected $guarded = [];

    public function getImageUrlAttribute()
    {
    	return Storage::disk('images')->url($this->image);
    }

    public function excerpt($chars = 70)
    {
        return str_limit(strip_tags($this->bio), $chars);
    }
}
