<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
    protected $guarded = [];

    public function getDownloadUrlAttribute()
    {
    	return Storage::disk('attachments')->url($this->path);
    }
}
