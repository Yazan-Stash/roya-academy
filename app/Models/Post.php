<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
	use SoftDeletes;
	
	protected $guarded = [];
	
	public function getFormattedCreatedAtAttribute()
	{	
		return $this->created_at->format('Y-M-d H:i');
	}

	public function url($relative = true)
	{
		if (! $relative) {
			return route('posts.show', ['post' => $this->id]);
		}

		return route('posts.show', ['post' => $this->id], false);
	}

	public function getImageUrlAttribute()
    {
    	return Storage::disk('images')->url($this->image);
    }

    public function excerpt($chars = 70)
    {
        return str_limit(
            html_entity_decode(
                strip_tags($this->body)
            ), 
            $chars
        );
    }

    public function scopePublished($query)
    {
    	return $query->where('published', 1);
    }
}
