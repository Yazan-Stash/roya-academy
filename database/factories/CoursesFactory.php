<?php

use App\Staff;
use App\Course;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;

$factory->define(App\Course::class, function (Faker $faker) {

    $categories = ['Certificate of Skills', 'Diploma'];
	$statuses = ['حالياً', 'قريباً', 'انتهت'];

    return [
        'title' => $faker->sentence(1),
        'description' => $faker->paragraph(2),
        'category' => $faker->randomElement($categories),
        'status' => $faker->randomElement($statuses),
        'price' => $faker->numberBetween(200, 4500),
        'duration' => $faker->numberBetween(3, 48) . ' ' . $faker->word,
        'date' => Carbon::now()->addDays(3)->format('D, F s'),
        'seats' => $faker->numberBetween(70, 150),
        'image' => 'image/path',
    ];
});

$factory->state(App\Course::class, 'with_instructors', function (Faker $faker) {
  
    $instructors = factory(Staff::class, 3)->create();
  
    return [
        'instructors' => $instructors->pluck('id'),
    ];
});
