<?php

use App\Attachment;
use Faker\Generator as Faker;

$factory->define(Attachment::class, function (Faker $faker) {
    return [
    	'title' => $faker->word,
        'path' => 'some_attachment_path'
    ];
});
