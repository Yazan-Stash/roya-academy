<?php

use App\Guest;
use Faker\Generator as Faker;

$factory->define(Guest::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2),
        'image' => 'path/to/image.png',
        'bio' => $faker->sentence(3)
    ];
});
