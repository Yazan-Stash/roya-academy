<?php

use App\Post;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(1),
        'body' => $faker->paragraph(3),
        'slug' => $faker->slug(1),
        'image' => $faker->imageUrl(1920, 1080),
        'published' => 1,
    ];
});
