<?php

use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(App\Staff::class, function (Faker $faker) {
    return [
        'name' => str_replace("'", '', $faker->name),
        'title' => $faker->title,
        'bio' => $faker->paragraph,
        'image' => $faker->imageUrl(1080, 1080),
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ];
});

$factory->state(App\Staff::class, 'image_file', [
    'image' => UploadedFile::fake()->image('avatar.jpg', 600, 600)->size(99)
]);
