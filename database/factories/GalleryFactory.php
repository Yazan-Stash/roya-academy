<?php

use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(App\Gallery::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
    ];
});

$factory->state(App\Gallery::class, 'youtube_url', function (Faker $faker) {
	return [
	    'youtube_url' => "https://www.youtube.com/watch?v=MXzKLpYTfBE",
	];
});

$factory->state(App\Gallery::class, 'image_url', function (Faker $faker) {
	return [
		'path' => 'http://example.com/image.jpg',
	    'youtube_url' => null
	];
});

$factory->state(App\Gallery::class, 'video_file', function (Faker $faker) {
	return [
		'file' => UploadedFile::fake()->create('video.mp4', 15000)
	];
});

$factory->state(App\Gallery::class, 'image_file', function (Faker $faker) {
	return [
		'file' => UploadedFile::fake()->image('avatar.jpg', 1920, 1080)->size(99)
	];
});
