<?php

use App\Slider;
use Faker\Generator as Faker;
use Illuminate\Http\UploadedFile;

$factory->define(Slider::class, function (Faker $faker) {
    return [
        'heading' => $faker->sentence(2),
        'caption' => $faker->paragraph(2),
        'image' => $faker->imageUrl(1920, 1080),
        'link' => $faker->imageUrl(1920, 1080),
    ];
});
