<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('category');
            $table->string('status');
            $table->integer('price')->nullable()->unsigned();
            $table->string('duration')->nullable();
            $table->string('date')->nullable();
            $table->integer('seats')->nullable()->unsigned();
            $table->string('image');
            $table->timestamps();
        });

        Schema::create('course_staff', function (Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('staff_id')->unsigned();

            $table->foreign('course_id')
                ->references('id')->on('courses')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('staff_id')
                ->references('id')->on('staff')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_staff');
        Schema::dropIfExists('courses');
    }
}
