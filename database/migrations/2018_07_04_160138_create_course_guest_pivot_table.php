<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseGuestPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_guest', function (Blueprint $table) {
            $table->integer('course_id')->unsigned();
            $table->integer('guest_id')->unsigned();

            $table->foreign('course_id')
                ->references('id')->on('courses')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('guest_id')
                ->references('id')->on('guests')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_guest', function(Blueprint $table){
            $table->dropForeign(['course_id']);
            $table->dropForeign(['guest_id']);
        });

        Schema::dropIfExists('course_guest');
    }
}
