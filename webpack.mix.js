const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

/** Theme Styles **/
mix.styles([
    'public/theme/css/bootstrap-rtl.min.css',
    'public/theme/css/animate.css',
    'public/theme/css/owl.carousel.css',
    'public/theme/css/slick.css',
    'public/theme/css/magnific-popup.css',
    'public/theme/css/off-canvas.css',
    'public/theme/css/rsmenu-main.css',
    'public/theme/css/rsmenu-transitions.css',
    'public/theme/css/responsive.css'
    ],'public/css/app.css').version();

/** Theme Fonts **/
mix.styles([
    'public/theme/fonts/flaticon.css',
    'public/theme/fonts/fonts2/flaticon.css'
    ],'public/css/fonts.css').version();

/** Admin Styles **/
mix.styles([
    'public/bower_components/Ionicons/css/ionicons.min.css',
    'public/bower_components/admin-lte/dist/css/AdminLTE.css',
    'public/bower_components/admin-lte/dist/css/skins/skin-blue.css',
    'public/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
    'public/bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.css',
    'public/bower_components/select2/dist/css/select2.css'
],'public/css/admin.css').version();

/** Admin Scripts **/
mix.js([
    'public/bower_components/admin-lte/dist/js/adminlte.min.js',
    'public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    'public/bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.js',
    'public/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js',
    'resources/assets/js/admin.js'
],'public/js/admin.js').version();
