<?php

/*
    |--------------------------------------------------------------------------
    | Environment Key-Value Pairs
    |--------------------------------------------------------------------------
    |
    | Here you may specify env key-value pairs.. added benefit to directly 
    | using env() is config caching. can be used like config('env.key')
    |
    */
   
return [
	'image_cdn' => env('IMAGE_CDN', 'http://academy.roya.tv/storage/images/')
];
