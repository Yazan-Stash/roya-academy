<?php

namespace Tests\Unit;

use App\Attachment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AttachmentTest extends TestCase
{
    /** @test */
    public function generates_fully_qualified_url()
    {
    	$attachment = factory(Attachment::class)->make();

    	$this->assertTrue(
    		str_contains(
    			$attachment->download_url, 
    			'storage/attachments/' . $attachment->path
    		)
    	);
    	
    	$this->assertTrue(
    		str_contains(
    			$attachment->download_url, 
    			config('app.url')
    		)
    	);
    }
}
