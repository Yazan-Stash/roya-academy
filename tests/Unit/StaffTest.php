<?php

namespace Tests\Unit;

use App\Staff;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaffTest extends TestCase
{
    /** @test */
    public function it_generates_a_fully_qualified_image_url()
    {
    	$staff = factory(Staff::class)->make(['image' => 'path/to/image.jpg']);

    	$this->assertSame(
    		config('app.url') . '/storage/images/' . $staff->image,
    		$staff->image_url
    	);
    }

    /** @test */
    public function it_generates_an_excerpt_from_the_bio()
    {
        $staff = factory(Staff::class)->make(['bio' => "<p>some html text here, which should be long enough to trim</p>"]);

        $this->assertSame(
            'some html text here, which should be lon...',
            $staff->excerpt(40)
        );
    }
}
