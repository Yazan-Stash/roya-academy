<?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_formats_the_created_at_date()
    {
        $post = factory(Post::class)->create();

    	$this->assertSame($post->created_at->format('Y-M-d H:i'), $post->formatted_created_at);
    }

    /** @test */
    public function it_generates_a_relative_link_for_a_post()
    {
    	$post = factory(Post::class)->create();

    	$this->assertSame(
    		"/posts/{$post->id}", 
    		$post->url()
    	);
    }

    /** @test */
    public function it_generates_an_absolute_link_for_a_post()
    {
    	$post = factory(Post::class)->create();

    	$this->assertSame(
    		config('app.url') . "/posts/{$post->id}", 
    		$post->url(false)
    	);
    }

    /** @test */
    public function it_soft_deletes_posts()
    {
        $post = factory(Post::class)->create();
        $post->delete();
        
        $this->assertEquals(0, Post::count());
        $this->assertEquals(1, Post::withTrashed()->count());
    }

    /** @test */
    public function it_generates_a_fully_qualified_image_url()
    {
        $post = factory(Post::class)->make(['image' => 'path/to/image.jpg']);

        $this->assertSame(
            config('app.url') . '/storage/images/' . $post->image,
            $post->image_url
        );
    }

    /** @test */
    public function it_generates_an_excerpt_from_the_body()
    {
        $post = factory(Post::class)->make(['body' => "<p>some html text here, which should be long enough to trim</p>"]);

        $this->assertSame(
            'some html text here, which should be lon...',
            $post->excerpt(40)
        );
    }

    /** @test */
    public function it_scopes_posts_depending_on_the_published_property()
    {
        $published_post = factory(Post::class)->create(['published' => '1']);
        $unpublished_post = factory(Post::class)->create(['published' => '0']);

        $this->assertCount(2, Post::all());
        $this->assertCount(1, Post::published()->get());
    }
}
