<?php

namespace Tests\Unit;

use App\Guest;
use App\Staff;
use App\Course;
use App\Attachment;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function a_course_has_many_instructors()
    {
		$staff = factory(Staff::class, 3)->create();
    	$course = factory(Course::class)->create();

    	$course->instructors()->attach($staff->pluck('id'));

		$this->assertEquals(3, $course->instructors()->count());
    }

    /** @test */
    public function a_course_has_many_guests()
    {
        $guests = factory(Guest::class, 3)->create();
        $course = factory(Course::class)->create();

        $course->guests()->attach($guests->pluck('id'));

        $this->assertEquals(3, $course->guests()->count());
    }

    /** @test */
    public function a_course_has_many_attachments()
    {
        $course = factory(Course::class)->create();

        $course->attachments()->create([
            'title' => 'some title here', 
            'path' => factory(Attachment::class)->make()->path, 
        ]);
        $course->attachments()->create([
            'title' => 'some title here', 
            'path' => factory(Attachment::class)->make()->path, 
        ]);

        $this->assertEquals(2, $course->attachments()->count());
    }

    /** @test */
    public function it_generates_a_fully_qualified_image_url()
    {
        $course = factory(Course::class)->make(['image' => 'path/to/image.jpg']);

        $this->assertSame(
            config('app.url') . '/storage/images/' . $course->image,
            $course->image_url
        );
    }

    /** @test */
    public function it_generates_an_excerpt_from_the_description()
    {
        $course = factory(Course::class)->make(['description' => "<p>some html text here, which should be long enough to trim</p>"]);

        $this->assertSame(
            'some html text here, which should be lon...',
            $course->excerpt(40)
        );
    }
}
