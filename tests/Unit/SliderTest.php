<?php

namespace Tests\Unit;

use App\Slider;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SliderTest extends TestCase
{
    /** @test */
    public function it_generates_a_fully_qualified_image_url()
    {
    	$slider = factory(Slider::class)->make(['image' => 'path/to/image.jpg']);

    	$this->assertSame(
    		config('app.url') . '/storage/images/' . $slider->image,
    		$slider->image_url
    	);
    }

    /** @test */
    public function it_generates_an_excerpt_from_the_caption()
    {
        $slider = factory(Slider::class)->make(['caption' => "<p>some html text here, which should be long enough to trim</p>"]);

        $this->assertSame(
            'some html text here, which should be lon...',
            $slider->excerpt(40)
        );
    }

    /** @test */
    public function it_returns_a_hash_if_link_is_not_set()
    {
        $slider = factory(Slider::class)->make(['link' => null]);
            
        $this->assertSame('#', $slider->link);
    }
}
