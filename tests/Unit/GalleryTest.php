<?php

namespace Tests\Unit;

use App\Gallery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GalleryTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_can_determine_if_the_video_is_hosted_on_youtube()
    {
    	$gallery = factory(Gallery::class)->states('youtube_url')->create();

    	$this->assertTrue($gallery->isYoutube());
    }

    /** @test */
    public function it_can_determine_if_the_video_is_hosted_on_our_servers()
    {
    	$gallery = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage']);

    	$this->assertTrue($gallery->isNotYoutube());
    }

    /** @test */
    public function it_can_determine_if_the_file_is_an_image()
    {
    	$gallery_jpg = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage.jpg']);
    	$gallery_jpeg = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage.jpeg']);
    	$gallery_png = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage.png']);
    	$gallery_other = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage.other']);

    	$this->assertTrue($gallery_jpg->isImage());
    	$this->assertTrue($gallery_jpeg->isImage());
    	$this->assertTrue($gallery_png->isImage());
    	$this->assertFalse($gallery_other->isImage());
    }

    /** @test */
    public function it_can_determine_if_the_file_is_a_video()
    {
    	$gallery_mp4 = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage.mp4']);
    	$gallery_other = factory(Gallery::class)->create(['path' => 'some-imaginery-path-on-storage.other']);

    	$this->assertTrue($gallery_mp4->isVideo());
    	$this->assertFalse($gallery_other->isVideo());
    }

    public function it_generates_a_fully_qualified_asset_url()
    {
        $gallery_image = factory(Gallery::class)->make(['path' => 'path/to/image.jpg']);
        $this->assertSame(
            config('app.url') . '/storage/images/' . $gallery_image->path,
            $gallery_image->asset_url
        );

        $gallery_video = factory(Gallery::class)->make(['path' => 'path/to/image.mp4']);
        $this->assertSame(
            config('app.url') . '/storage/gallery/' . $gallery_video->path,
            $gallery_video->asset_url
        );
    }

    /** @test */
    public function it_scopes_results_to_only_images()
    {
        $image = factory(Gallery::class)->create(['path' => 'image.jpg']);
        $video = factory(Gallery::class)->create(['path' => 'video.mp4']);

        $this->assertCount(1, Gallery::imagesOnly()->get());
    }
}
