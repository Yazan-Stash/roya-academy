<?php

namespace Tests\Unit;

use App\Guest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GuestTest extends TestCase
{
    /** @test */
    public function it_generates_a_fully_qualified_image_url()
    {
    	$guest = factory(Guest::class)->make(['image' => 'path/to/image.jpg']);

    	$this->assertSame(
    		config('app.url') . '/storage/images/' . $guest->image,
    		$guest->image_url
    	);
    }

    /** @test */
    public function it_generates_an_excerpt_from_the_bio()
    {
        $guest = factory(Guest::class)->make(['bio' => "<p>some html text here, which should be long enough to trim</p>"]);

        $this->assertSame(
            'some html text here, which should be lon...',
            $guest->excerpt(40)
        );
    }
}
