<?php

namespace Tests\Feature;

use App\Course;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\ViewComposers\LatestCoursesSidebarComposer;

class LatestCoursesSidebarTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_assert_the_view_composer_is_loading_data_into_the_partial_view()
    {
    	$view = app('Illuminate\Contracts\View\Factory')->make('includes.latest-courses');
        $composer= (new LatestCoursesSidebarComposer())->compose($view);

        $this->assertTrue(
            isset($view['latest_courses'])
        );
    }

    /** @test */
    public function it_asserts_the_loaded_data_are_the_latest_courses()
    {
    	$courses = factory(Course::class, 5)->create();

    	$view = app('Illuminate\Contracts\View\Factory')->make('includes.latest-courses');
        $composer= (new LatestCoursesSidebarComposer())->compose($view);

        // Assert latest courses are actually loaded
        $this->assertEquals(
        	$courses->sortBy('created_at')->toArray(), 
        	$view['latest_courses']->toArray()
        );

        // Assert the first course in the list is the latest course in the list
        $this->assertTrue(
        	$view['latest_courses'][0]->created_at
        		->gte($view['latest_courses'][4]->created_at)
        	);
    }

    /** @test */
    public function it_asserts_loaded_courses_are_at_most_five()
    {
    	$courses = factory(Course::class, 10)->create();

    	$view = app('Illuminate\Contracts\View\Factory')->make('includes.latest-courses');
        $composer= (new LatestCoursesSidebarComposer())->compose($view);

        $this->assertLessThanOrEqual(5,$view['latest_courses']->count());
    }
}
