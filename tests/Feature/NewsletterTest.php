<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\NewsletterSubscriber;
use App\Mail\SubscribedToNewsletter;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NewsletterTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_validates_the_email_before_subscribing()
    {
        // Email field is missing
        $response = $this->json('POST', 'api/newsletter/subscribe', []);
        $response->assertJsonValidationErrors('email');
        $response->assertStatus(422);

        // Email is invalid
        $response = $this->json('POST', 'api/newsletter/subscribe', ['email' => 'some-invalid-email-address']);
        $response->assertJsonValidationErrors('email');
        $response->assertStatus(422);

        // Email is already subscribed
        $subscriber = factory(NewsletterSubscriber::class)->create();
        $response = $this->json('POST', 'api/newsletter/subscribe', ['email' => $subscriber->email]);
        $response->assertJsonValidationErrors('email');
        $response->assertStatus(422);
    }

    /** @test */
    public function it_subscribes_an_email_to_the_newsletter()
    {
    	$email = 'email@example.com';

    	$response = $this->json('POST', 'api/newsletter/subscribe', ['email' => $email]);

    	$response->assertJson(['message' => 'تم إرسال طلبك، يرجى مراجعة بريدك الإلكتروني']);
    	$this->assertDatabaseHas('newsletter_subscribers', ['email' => $email]);
    	$this->assertNotNull(NewsletterSubscriber::first()->activation_code);
    }

    /** @test */
    public function it_sends_email_activation_email()
    {
    	Mail::fake();

    	$this->json('POST', 'api/newsletter/subscribe', ['email' => 'email@example.com']);

    	Mail::assertQueued(SubscribedToNewsletter::class, function($mail){
    		return $mail->subscriber->email === 'email@example.com'; 
    	});
    }

    /** @test */
    public function it_activates_subscribers_email()
    {
    	$subscriber = factory(NewsletterSubscriber::class)->create();
    	
    	$response = $this->get(
    		route('newsletter.activate-email', [
    			'code' => $subscriber->activation_code
    		]
    	));

        $response->assertSee($subscriber->email);
    	$response->assertSee('لقد تم تأكيد إشتراكك بالنشرة الإخبارية لأكاديمية رؤيا للتدريب الإعلامي');
    }
}
