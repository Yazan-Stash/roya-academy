<?php

namespace Tests\Feature;

use App\Post;
use App\Staff;
use App\Course;
use App\Slider;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomepageTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function it_can_see_slider_images()
	{
		$sliders = factory(Slider::class, 3)->create();

		$response = $this->get('/');

		$sliders->each(function($slider) use ($response){
            $response->assertSee((string)$slider->url);
		});
	}

	/** @test */
	public function it_can_see_at_most_ten_slider_images()
	{
		$staff = factory(Slider::class, 12)->create();

		$response = $this->get('/');

		$view_data = $response->getOriginalContent()->getData();

		$this->assertLessThanOrEqual(10, count($view_data['sliders']));
	}

	/** @test */
	public function it_can_see_courses()
	{
		$courses = factory(Course::class, 5)->create();

		$response = $this->get('/');

		$courses->each(function($course) use ($response){
			$response->assertSee($course->category);
			$response->assertSee($course->title);
			$response->assertSee(str_limit($course->description, 30, ''));
		});
	}

	/** @test */
	public function it_can_see_at_most_five_courses()
	{
		$courses = factory(Course::class, 5)->create();

		$response = $this->get('/');

		$view_data = $response->getOriginalContent()->getData();

		$this->assertLessThanOrEqual(5, count($view_data['courses']));
	}

	/** @test */
	public function it_can_see_staff_members()
	{
		$staff = factory(Staff::class, 5)->create();

		$response = $this->get('/');

		$staff->each(function($staff_member) use ($response){
			$response->assertSee($staff_member->name);
			$response->assertSee($staff_member->title);
		});
	}

	/** @test */
	public function it_can_see_at_most_five_staff_members()
	{
		$staff = factory(Staff::class, 10)->create();

		$response = $this->get('/');

		$view_data = $response->getOriginalContent()->getData();

		$this->assertLessThanOrEqual(5, count($view_data['staff']));
	}

	/** @test */
	public function it_can_see_only_published_posts()
	{
		$posts = factory(Post::class, 2)->create();
		$unpublished_post = factory(Post::class)->create(['published' => '0']);

		$response = $this->get('/');

		$posts->each(function($post) use ($response){
			$response->assertSee($post->title);
			$response->assertSee(str_limit($post->body, 30, ''));
			$response->assertSee($post->formatted_created_at);
		});

		$response->assertDontSee($unpublished_post->title);
		$response->assertDontSee(str_limit($unpublished_post->body, 30, ''));
	}

	/** @test */
	public function it_can_see_at_most_four_posts()
	{
		$staff = factory(Post::class, 10)->create();

		$response = $this->get('/');

		$view_data = $response->getOriginalContent()->getData();

		$this->assertLessThanOrEqual(4, count($view_data['posts']));
	}
}
