<?php

namespace Tests\Feature;

use App\Staff;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaffTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function it_can_see_a_list_of_staff_members()
	{
		$staff = factory(Staff::class, 12)->create();

		$response = $this->get('staff');

		$staff->each(function($staff_member) use ($response){
			$response->assertSee($staff_member->name);
			$response->assertSee($staff_member->title);
		});
	}

	/** @test */
	public function it_orders_staff_members_in_ascending_order()
	{
		$first_staff = factory(Staff::class)->create(['created_at' => \Carbon\Carbon::now()->addDays(5)]);
		$second_staff = factory(Staff::class)->create(['created_at' => \Carbon\Carbon::now()->addDays(1)]);

		$response = $this->get('staff');
		$view_data = $this->getViewData($response, 'staff');

		$this->assertEquals(1, $view_data[0]->id);
		$this->assertEquals(2, $view_data[1]->id);
	}

	/** @test */
	public function it_can_see_a_staff_member_details()
	{
		$staff = factory(Staff::class)->create();

		$response = $this->get("staff/{$staff->id}");

		$response->assertSee($staff->name);
		$response->assertSee($staff->bio);
		$response->assertSee($staff->email);
		$response->assertSee($staff->phone);
	}
}
