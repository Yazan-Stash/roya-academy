<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_can_see_published_posts_list_only()
    {
    	$posts = factory(Post::class, 5)->create();
    	$unpublished_posts = factory(Post::class)->create(['published' => '0']);

    	$response = $this->get('/posts');

    	$posts->each(function($post) use ($response){
	    	$response->assertSee($post->title);
	    	$response->assertSee(str_limit($post->body, 30, ''));
	    	$response->assertSee($post->formatted_created_at);
	    	$response->assertSee($post->url());
    	});

    	$response->assertDontSee($unpublished_posts->title);
    	$response->assertDontSee(str_limit($unpublished_posts->body, 30, ''));
    }

    /** @test */
	public function it_can_see_at_most_ten_posts()
	{
		$posts = factory(Post::class, 10)->create();

		$response = $this->get('posts');

		$view_data = $response->getOriginalContent()->getData();

		$this->assertLessThanOrEqual(10, count($view_data['posts']));
	}

	/** @test */
	public function is_can_see_a_post_with_its_details()
	{
		$post = factory(Post::class)->create();

		$response = $this->get("posts/{$post->id}");

		$response->assertSee($post->title);
		$response->assertSee($post->body);
		$response->assertSee($post->formatted_created_at);
	}

	/** @test */
	public function is_can_not_see_a_post_if_its_unpublished()
	{
		$post = factory(Post::class)->create(['published' => '0']);

		$response = $this->get("posts/{$post->id}");

		$response->assertDontSee($post->title);
		$response->assertDontSee($post->body);
		$response->assertDontSee($post->formatted_created_at);
	}
}
