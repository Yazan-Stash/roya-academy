<?php

namespace Tests\Feature;

use App\Staff;
use App\Course;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CoursesTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function it_can_see_a_list_of_courses()
	{
		$courses = factory(Course::class, 10)->create();

		$response = $this->get('courses');

		$courses->each(function($course) use ($response){
			$response->assertSee($course->title);
			$response->assertSee(str_limit($course->description, 30, ''));
			$response->assertSee($course->category);
		});
	}

	/** @test */
	public function it_can_see_a_course_details()
	{
		$course = factory(Course::class)->create();

		$response = $this->get("courses/{$course->id}");

		$response->assertSee($course->title);
		$response->assertSee($course->description);
		$response->assertSee($course->category);
		$response->assertSee((string)$course->price);
		$response->assertSee((string)$course->duration);
		$response->assertSee((string)$course->seats);
	}

	/** @test */
	public function it_can_see_course_instructors_details_on_the_course_details_page()
	{
		$staff = factory(Staff::class, 3)->create();
		$course = factory(Course::class)->create();
		$course->instructors()->attach($staff->pluck('id'));

		$response = $this->get("courses/{$course->id}");

		$staff->each(function($staff_member) use ($response){
			$response->assertSee($staff_member->name);
			$response->assertSee($staff_member->title);
		});
	}
}
