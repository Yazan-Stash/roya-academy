<?php

namespace Tests\Feature;

use App\Admin;
use App\Staff;
use Tests\TestCase;
use App\Jobs\OptimizeImage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaffAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');

        $this->disableImageUpload();
    }
    
    /** @test */
    public function it_can_see_a_list_of_staff_members()
    {
    	$staff = factory(Staff::class, 10)->create();

    	$response = $this->get('admins/staff');

    	$staff->each(function($staff_member) use ($response){
    		$response->assertSee($staff_member->name);
    	});
    }

    /** @test */
    public function it_can_create_new_staff_members()
    {
        $this->expectsJobs(OptimizeImage::class);

    	$staff = factory(Staff::class)->make();

    	$response = $this->post('admins/staff', $staff->toArray());

    	$response->assertStatus(302);
    	$response->assertRedirect('admins/staff');

    	$staff_from_database = Staff::first();

    	$this->assertEquals($staff->name , $staff_from_database->name);
    	$this->assertEquals($staff->title , $staff_from_database->title);
    	$this->assertEquals($staff->email , $staff_from_database->email);
    	$this->assertEquals($staff->phone , $staff_from_database->phone);
    	$this->assertEquals($staff->bio , $staff_from_database->bio);
    }

    /** @test */
    public function it_can_update_existing_staff_members()
    {
        $this->expectsJobs(OptimizeImage::class);
        
    	$staff_from_factory = factory(Staff::class)->states('image_file')->make();
        $this->post('admins/staff', $staff_from_factory->toArray());

        $old_image_path = Staff::first()->image;

        $staff_from_factory->name = 'new name';
        $staff_from_factory->title = 'new title';
        $staff_from_factory->email = 'new@email.com';
        $staff_from_factory->phone = '456789123';
        $staff_from_factory->bio = 'new bio';

    	$response = $this->patch("admins/staff/" . Staff::first()->id, $staff_from_factory->toArray());

    	$response->assertStatus(302);
    	$response->assertRedirect('admins/staff');

    	$staff_from_database = Staff::first();

    	$this->assertEquals($staff_from_factory->name , $staff_from_database->name);
    	$this->assertEquals($staff_from_factory->title , $staff_from_database->title);
    	$this->assertEquals($staff_from_factory->email , $staff_from_database->email);
    	$this->assertEquals($staff_from_factory->phone , $staff_from_database->phone);
    	$this->assertEquals($staff_from_factory->bio , $staff_from_database->bio);
    }

    /** @test */
    public function it_validates_staff_create_requests()
    {
        // name, title, bio and image fields are required
    	$response = $this->post('admins/staff', []);

        $response->assertRedirect();
    	$response->assertSessionHasErrors(['name', 'title', 'bio', 'image']);

        // name and title are not longer than 55 characters
        $staff = factory(Staff::class)->make(['name' => str_random(56), 'title' => str_random(56)]);

        $response = $this->post('admins/staff', $staff->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors(['name', 'title']);

        // email field must be a valid email
        $staff = factory(Staff::class)->make(['email' => 'just-an-invalid-email']);

        $response = $this->post('admins/staff', $staff->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function it_validates_staff_update_requests()
    {
        // name, title and bio fields are required
        $staff = factory(Staff::class)->create();
        unset($staff->name, $staff->title, $staff->bio);

        $response = $this->patch("admins/staff/{$staff->id}", $staff->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors(['name', 'title', 'bio']);

        // name and title are not longer than 55 characters
        $staff = factory(Staff::class)->create();
        $staff->name = str_random(56);
        $staff->title = str_random(56);

        $response = $this->patch("admins/staff/{$staff->id}", $staff->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors(['name', 'title']);

        // email field must be a valid email
        $staff = factory(Staff::class)->create();
        $staff->email = 'just-an-invalid-email';

        $response = $this->patch("admins/staff/{$staff->id}", $staff->toArray());

        $response->assertRedirect();
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function it_deletes_staff_members()
    {
        $staff = factory(Staff::class)->states('image_file')->make();
        $this->post('admins/staff', $staff->toArray());
        
        $old_image_path = Staff::first()->image;

        $response = $this->delete("admins/staff/" . $staff = Staff::first()->id);

        $response->assertRedirect('admins/staff');
        $this->assertEquals(0, Staff::count());

        Storage::fake();
        Storage::disk('images')->assertMissing($old_image_path);
    }
}
