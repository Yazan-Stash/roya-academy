<?php

namespace Tests\Feature;

use App\Admin;
use App\Staff;
use App\Course;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseCreateValidationAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');
    }

    /** @test */
    public function it_validates_required_fields()
    {
        $response = $this->post('admins/courses', []);

        $response->assertSessionHasErrors(['title', 'description', 'category', 'image', 'status']);
    }

    /** @test */
    public function it_validates_numeric_fields()
    {
        $course = factory(Course::class)->make();
        $response = $this->post('admins/courses', array_merge(
            $course->toArray(), ['price' => 'non-numeric value', 'seats' => 'non-numeric value',]
        ));

        $response->assertSessionHasErrors(['price', 'seats']);
    }

    /** @test */
    public function it_validates_fields_length()
    {
        $course = factory(Course::class)->make();
        $response = $this->post('admins/courses', array_merge(
            $course->toArray(), ['title' => str_random(256), 'description' => str_random(201), 'category' => str_random(56),]
        ));

        $response->assertSessionHasErrors(['title', 'category']);
    }

    /** @test */
    public function it_validates_status_value()
    {
        $course = factory(Course::class)->make(['status' => 'non-valid-status']);

        $response = $this->post('admins/courses', $course->toArray());

        $response->assertSessionHasErrors(['status']);
    }
}
