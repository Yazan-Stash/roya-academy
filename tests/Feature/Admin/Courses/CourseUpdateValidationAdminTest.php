<?php

namespace Tests\Feature;

use App\Admin;
use App\Staff;
use App\Course;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseUpdateValidationAdminTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');
    }

    /** @test */
    public function it_validates_required_fields_on_update()
    {
        $course = factory(Course::class)->create();
        unset($course->title, $course->description, $course->category, $course->image);
        
        $response = $this->patch("admins/courses/{$course->id}", $course->toArray());

        $response->assertSessionHasErrors(['title', 'description', 'category']);
    }

    /** @test */
    public function it_validates_numeric_fields_on_update()
    {
        $course = factory(Course::class)->create();
        $response = $this->patch("admins/courses/{$course->id}", array_merge(
            $course->toArray(), ['price' => 'non-numeric value', 'seats' => 'non-numeric value']
        ));

        $response->assertSessionHasErrors(['price', 'seats']);
    }

    /** @test */
    public function it_validates_fields_length_on_update()
    {
        $course = factory(Course::class)->create();
        $response = $this->patch("admins/courses/{$course->id}", array_merge(
            $course->toArray(), ['title' => str_random(256), 'description' => str_random(201), 'category' => str_random(56),]
        ));

        $response->assertSessionHasErrors(['title', 'category']);
    }

    /** @test */
    public function it_validates_status_value()
    {
        $course = factory(Course::class)->create();

        $response = $this->patch("admins/courses/{$course->id}", array_merge(
            $course->toArray(), ['status' => 'non-valid-status']
        ));

        $response->assertSessionHasErrors(['status']);
    }
}
