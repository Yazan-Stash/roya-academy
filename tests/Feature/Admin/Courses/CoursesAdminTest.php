<?php

namespace Tests\Feature;

use App\Admin;
use App\Staff;
use App\Course;
use Tests\TestCase;
use App\Jobs\OptimizeImage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CoursesAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');

        $this->disableImageUpload();
        $this->disableExceptionHandling();
    }

    /** @test */
    public function it_can_see_a_list_of_courses()
    {
    	$courses = factory(Course::class, 10)->create();

    	$response = $this->get('admins/courses');

    	$courses->each(function($course) use ($response){
    		$response->assertSee($course->title);
    		$response->assertSee($course->category);
    	});
    }

    /** @test */
    public function it_can_add_a_new_course_without_instructors()
    {
        $this->expectsJobs(OptimizeImage::class);

    	$course = factory(Course::class)->make();

    	$response = $this->post('admins/courses', $course->toArray());

    	$response->assertRedirect('admins/courses');

    	$course_from_db = Course::first();
    	$this->assertSame($course->title, $course_from_db->title);
    	$this->assertSame($course->description, $course_from_db->description);
    	$this->assertSame($course->category, $course_from_db->category);
        $this->assertEquals($course->status, $course_from_db->status);
    	$this->assertEquals($course->price, $course_from_db->price);
    	$this->assertEquals($course->duration, $course_from_db->duration);
        $this->assertEquals($course->seats, $course_from_db->seats);
    	$this->assertEquals($course->date, $course_from_db->date);
    }

    /** @test */
    public function it_attaches_instructors_when_creating_a_course()
    {
        $this->expectsJobs(OptimizeImage::class);

        $staff = factory(Staff::class, 3)->create();
        $course = factory(Course::class)->make();

        $response = $this->post('admins/courses', $course->toArray() + ['instructors' => ['1', '2', '3']]);

        $response->assertRedirect('admins/courses');
        $this->assertEquals(3, Course::first()->instructors()->count());
    }

    /** @test */
    public function it_can_update_a_course_without_changing_instructors()
    {
        $this->expectsJobs(OptimizeImage::class);

        factory(Staff::class, 2)->create();
        $course = factory(Course::class)->make(['status' => 'حالياً']);
        $this->post('admins/courses', $course->toArray() + ['instructors' => ['1', '2']]);

        $course = Course::first();
        
        $course->fill([
            'title' => 'new title',
            'description' => 'new desc',
            'category' => 'new categ',
            'price' => '1234',
            'status' => 'انتهت',
            'duration' => '4321 months',
            'date' => '20th of July',
            'image' => UploadedFile::fake()->image('avatar.jpg', 1920, 1080)->size(99),
            'seats' => '99',
            'instructors' => $course->instructors()->pluck('id')
        ]);

        $response = $this->patch("admins/courses/{$course->id}", $course->toArray());

        $response->assertRedirect('admins/courses');

        $course_from_db = Course::first();

        $this->assertSame($course->title, $course_from_db->title);
        $this->assertSame($course->description, $course_from_db->description);
        $this->assertSame($course->category, $course_from_db->category);
        $this->assertEquals($course->status, $course_from_db->status);
        $this->assertEquals($course->price, $course_from_db->price);
        $this->assertEquals($course->duration, $course_from_db->duration);
        $this->assertEquals($course->date, $course_from_db->date);
        $this->assertEquals($course->seats, $course_from_db->seats);
        $this->assertEquals(2, $course_from_db->instructors()->count());
    }

    /** @test */
    public function it_updates_instructors_on_update()
    {
        $this->expectsJobs(OptimizeImage::class);

        factory(Staff::class, 2)->create();
        $course = factory(Course::class)->create();

        $response = $this->patch("admins/courses/{$course->id}", $course->toArray() + ['instructors' => ['1', '2']]);

        $response->assertRedirect('admins/courses');

        $this->assertEquals(2, Course::first()->instructors()->count());
    }

    /** @test */
    public function it_detaches_instructors_on_update()
    {
        $this->expectsJobs(OptimizeImage::class);

        factory(Staff::class, 2)->create();
        $course = factory(Course::class)->create();
        $course->instructors()->sync([1, 2]);

        $response = $this->patch("admins/courses/{$course->id}", $course->toArray());

        $response->assertRedirect('admins/courses');

        $this->assertEquals(0, Course::first()->instructors()->count());
    }

    /** @test */
    public function it_deletes_courses()
    {
        $this->expectsJobs(OptimizeImage::class);
        
        $course = factory(Course::class)->states('with_instructors')->make();

        $this->post('admins/courses', $course->toArray());
        
        $old_image_path = Course::first()->image;

        $response = $this->delete("admins/courses/" . $course = Course::first()->id);

        $response->assertRedirect('admins/courses');
        $this->assertEquals(0, Course::count());
    }
}
