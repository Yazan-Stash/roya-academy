<?php

namespace Tests\Feature;

use App\Post;
use App\Admin;
use Tests\TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostUpdateValidationAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');
    }

    /** @test */
    public function it_validates_required_fields_on_update()
    {
    	$post = factory(Post::class)->create();
    	unset($post->title, $post->body, $post->image);
    	$response = $this->patch("admins/posts/{$post->id}", $post->toArray());
    	$response->assertSessionHasErrors(['title', 'body']);
    }

    /** @test */
    public function it_validates_fields_length_on_update()
    {
    	$post = factory(Post::class)->create();
    	$post->title = str_random(256);
    	$response = $this->patch("admins/posts/{$post->id}", $post->toArray());
    	$response->assertSessionHasErrors(['title']);
    }

    /** @test */
    public function it_validates_fields_types_on_update()
    {
    	$post = factory(Post::class)->create();
    	$post->published = 'non-boolean-value';

    	$response = $this->patch("admins/posts/{$post->id}", $post->toArray());
    	$response->assertSessionHasErrors(['published']);
    }
}
