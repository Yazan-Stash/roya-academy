<?php

namespace Tests\Feature;

use App\Post;
use App\Admin;
use Tests\TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostCreateValidationAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');
    }

    /** @test */
    public function it_validates_required_fields_on_create()
    {
    	$response = $this->post('admins/posts', []);
    	$response->assertSessionHasErrors(['title', 'body', 'image']);
    }

    /** @test */
    public function it_validates_fields_length_on_create()
    {
    	$post = factory(Post::class)->make(['title' => str_random(256)]);
    	$response = $this->post('admins/posts', $post->toArray());
    	$response->assertSessionHasErrors(['title']);
    }

    /** @test */
    public function it_validates_fields_types_on_create()
    {
    	$post = factory(Post::class)->make(['published' => 'non-boolean-value']);
    	$response = $this->post('admins/posts', $post->toArray());
    	$response->assertSessionHasErrors(['published']);
    }
}
