<?php

namespace Tests\Feature;

use App\Post;
use App\Admin;
use Tests\TestCase;
use App\Jobs\OptimizeImage;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $this->disableExceptionHandling();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');

        $this->disableImageUpload();
    }
    
    /** @test */
    public function it_can_see_a_list_of_posts()
    {
    	$posts = factory(Post::class, 5)->create();

    	$response = $this->get('admins/posts');

    	$posts->each(function($post) use ($response) {
    		$response->assertSee($post->title);
    	});
    }

    /** @test */
    public function it_can_add_new_post_and_generate_a_slug()
    {
        $this->expectsJobs(OptimizeImage::class);

    	$post = factory(Post::class)->make();

        $response = $this->post('admins/posts', $post->toArray());
    	$response->assertRedirect('admins/posts');

    	$post_from_db = Post::first();
    	$this->assertEquals($post->body, $post_from_db->body);
    	$this->assertEquals($post->title, $post_from_db->title);
    	$this->assertSame(str_slug($post->title), $post_from_db->slug);
    	$this->assertEquals((int)$post->published, $post_from_db->published);
    }

    /** @test */
    public function it_can_update_an_existing_post()
    {
        $this->expectsJobs(OptimizeImage::class);
        
    	$post = factory(Post::class)->make();
    	$this->post('admins/posts', $post->toArray());

    	$post = Post::first();
    	$old_image_path = $post->image;

    	$post->title = 'new title';
    	$post->body = 'new body';
    	$post->published = 1;
    	$post->image = UploadedFile::fake()->image('image.jpg', 1920, 1080)->size(99);

        $response = $this->patch("admins/posts/{$post->id}", $post->toArray());
        $response->assertRedirect('admins/posts');

    	$post_from_db = Post::first();
    	$this->assertEquals($post->body, $post_from_db->body);
    	$this->assertEquals($post->title, $post_from_db->title);
    	$this->assertSame($post->slug, $post_from_db->slug);
    	$this->assertEquals((int)$post->published, $post_from_db->published);
    }

    /** @test */
    public function it_soft_deletes_posts()
    {
        $this->expectsJobs(OptimizeImage::class);

    	$post = factory(Post::class)->make();
    	$this->post('admins/posts', $post->toArray());

    	$post = Post::first();
    	$response = $this->delete("admins/posts/{$post->id}");

    	$response->assertRedirect('admins/posts');

    	$this->assertEquals(0, Post::count());
    	$this->assertEquals(1, Post::withTrashed()->count());
    }
}
