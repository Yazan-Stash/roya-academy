<?php

namespace Tests\Feature;

use App\Admin;
use App\Slider;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SliderUpdateValidationAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');
    }

   	/** @test */
   	public function it_validates_slider_update_requests()
   	{
      // title is not longer than 55 characters, caption is not longer than 200 characters
   		$slider = factory(Slider::class)->create();
   		$slider->heading = str_random(56);
   		$slider->caption = str_random(201);

   		$response = $this->patch("admins/sliders/{$slider->id}", $slider->toArray());

   		$response->assertRedirect();
      $response->assertSessionHasErrors(['heading', 'caption']);
   	}
}
