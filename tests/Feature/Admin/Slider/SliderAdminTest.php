<?php

namespace Tests\Feature;

use App\Admin;
use App\Slider;
use Tests\TestCase;
use App\Jobs\OptimizeImage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SliderAdminTest extends TestCase
{
    use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');

        $this->disableImageUpload();
    }

    /** @test */
    public function it_can_see_a_list_of_sliders()
    {
        $sliders = factory(Slider::class, 5)->create();

        $response = $this->Get('admins/sliders');

        $sliders->each(function ($slider) use ($response) {
            $response->assertSee($slider->heading);
        });
    }

    /** @test */
    public function it_can_add_a_new_image_to_the_slider()
    {
        $this->expectsJobs(OptimizeImage::class);

        $slider = factory(Slider::class)->make();

        $response = $this->post('admins/sliders', $slider->toArray());

        $response->assertRedirect('admins/sliders');
        
        $slider_from_database = Slider::first();

        $this->assertSame($slider->heading, $slider_from_database->heading);
        $this->assertSame($slider->caption, $slider_from_database->caption);
        $this->assertSame($slider->link, $slider_from_database->link);
    }

    /** @test */
    public function it_can_update_an_existing_slider()
    {
        $this->expectsJobs(OptimizeImage::class);
        
        $slider = factory(Slider::class)->make();
        $response = $this->post('admins/sliders', $slider->toArray());

        $slider = Slider::first();
        $old_image_path = $slider->image;

        $slider->heading = 'new heading';
        $slider->caption = 'new caption';
        $slider->link = 'http://new-url.com/external-link';
        $slider->image = UploadedFile::fake()->image('slider.jpg', 1920, 1080)->size(99);

        $response = $this->patch("admins/sliders/{$slider->id}", $slider->toArray());

        $slider_from_database = Slider::first();

        $this->assertSame($slider->heading, $slider_from_database->heading);
        $this->assertSame($slider->caption, $slider_from_database->caption);
        $this->assertSame($slider->link, $slider_from_database->link);
    }

    /** @test */
    public function it_deletes_slider_images_when_there_is_more_than_two_images_remaining()
    {
        // Make sure there is more than two other images to satisfy the deletion policy
        factory(Slider::class, 2)->create();

        $slider = factory(Slider::class)->make();
        $this->post('admins/sliders', $slider->toArray());
            
        $slider = Slider::first();

        $old_image_path = $slider->image;

        $response = $this->delete("admins/sliders/" . $slider->id);

        $response->assertRedirect('admins/sliders');
        $this->assertEquals(2, Slider::count()); // make sure we only have the original two images
    }

    /** @test */
    public function it_cant_delete_an_image_if_the_remaining_images_are_less_than_2()
    {
        $sliders = factory(Slider::class, 2)->create();

        $response = $this->delete("admins/sliders/{$sliders[0]->id}");

        $this->assertCount(2, Slider::all());
    }

    /** @test */
    public function it_stores_at_most_five_sliders()
    {
        $sliders = factory(Slider::class, 5)->create();

        $slider = factory(Slider::class)->make();
        $response = $this->post('admins/sliders', $slider->toArray());

        $this->assertEquals(5, Slider::count());
    }
}
