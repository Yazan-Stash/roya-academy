<?php

namespace Tests\Feature;

use App\Admin;
use App\Slider;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SliderCreateValidationAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');
    }

   	/** @test */
   	public function it_validates_slider_create_requests()
   	{
   		// image field is missing
   		$response = $this->post('admins/sliders', []);

   		$response->assertRedirect();
   		$response->assertSessionHasErrors(['image']);

   		// title is not longer than 55 characters, caption is not longer than 200 characters
   		$slider = factory(Slider::class)->make(['heading' => str_random(56), 'caption' => str_random(201)]);
   		$response = $this->post('admins/sliders', $slider->toArray());

   		$response->assertRedirect();
      $response->assertSessionHasErrors(['heading', 'caption']);
   	}
}
