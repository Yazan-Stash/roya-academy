<?php

namespace Tests\Feature;

use App\Admin;
use App\Guest;
use Tests\TestCase;
use App\Jobs\OptimizeImage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GuestsAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');

        $this->disableImageUpload();
        // $this->disableExceptionHandling();
    }

    /** @test */
    public function it_can_see_a_list_of_guests()
    {
    	$guests = factory(Guest::class, 3)->create();

    	$response = $this->get('admins/guests');

    	$guests->each(function($guest) use ($response){
    		$response->assertSee($guest->name);
    	});
    }

    /** @test */
    public function it_can_create_new_guest()
    {
        $this->expectsJobs(OptimizeImage::class);

    	$guest = factory(Guest::class)->make();

    	$response = $this->post('admins/guests', $guest->toArray());

    	$response->assertRedirect('admins/guests');

    	$guest_from_database = Guest::first();
    	$this->assertEquals($guest->name , $guest_from_database->name);
    	$this->assertEquals($guest->bio , $guest_from_database->bio);
    }

    /** @test */
    public function it_can_update_existing_staff_members()
    {
        $this->expectsJobs(OptimizeImage::class);

    	$guest_from_factory = factory(Guest::class)->make();
        $this->post('admins/guests', $guest_from_factory->toArray());

        $guest_from_factory->name = 'new name';
        $guest_from_factory->bio = 'new bio';

    	$response = $this->patch("admins/guests/" . Guest::first()->id, $guest_from_factory->toArray());

    	$response->assertRedirect('admins/guests');

    	$guest_from_database = Guest::first();

    	$this->assertEquals($guest_from_factory->name , $guest_from_database->name);
    	$this->assertEquals($guest_from_factory->bio , $guest_from_database->bio);
    }

    /** @test */
    public function it_validates_required_fields_on_create()
    {
    	$response = $this->post('admins/guests', []);
    	$response->assertSessionHasErrors(['name', 'image']);
    }

    /** @test */
    public function it_validates_name_length_on_create()
    {
        $guests = factory(Guest::class)->make(['name' => str_random(56)]);

        $response = $this->post('admins/guests', $guests->toArray());

        $response->assertSessionHasErrors(['name']);    	
    }

    /** @test */
    public function it_validates_required_fields_on_update()
    {
    	$guest = factory(Guest::class)->create();

    	$response = $this->patch("admins/guests/{$guest->id}", []);
    	
    	$response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function it_validates_name_length_on_update()
    {
        $guests = factory(Guest::class)->make(['name' => str_random(56)]);

        $response = $this->post('admins/guests', $guests->toArray());

        $response->assertSessionHasErrors(['name']);    	
    }

    /** @test */
    public function it_deletes_guests()
    {        
        $guest = factory(Guest::class)->create();
        $old_image_path = Guest::first()->image;

        $response = $this->delete("admins/guests/" . $guest->id);

        $response->assertRedirect('admins/guests');
        $this->assertEquals(0, Guest::count());

        Storage::fake();
        Storage::disk('images')->assertMissing($old_image_path);
    }
}
