<?php

namespace Tests\Feature;

use App\Admin;
use App\Gallery;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GalleryAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();

        $this->actingAs($admin, 'admin');
    }
    
    /** @test */
    public function it_can_see_a_list_of_gallery_images_and_videos()
    {
    	$gallery = factory(Gallery::class, 3)->states('youtube_url')->create();

    	$response = $this->get('admins/gallery');

    	$gallery->each(function($gallery_image) use ($response){
	    	$response->assertSee($gallery_image->title);
    	});
    }

    /** @test */
    public function it_can_add_a_new_video_with_youtube_link()
    {
    	$response = $this->post('admins/gallery', ['title' => 'video title', 'type' => 'youtube', 'youtube_url' => 'https://youtube.com/embed/asdasd']);

    	$video = Gallery::first();

    	$this->assertNotNull($video);
    	$this->assertSame('video title', $video->title);
    	$this->assertSame('https://youtube.com/embed/asdasd', $video->youtube_url);
    }

    /** @test */
    public function it_can_add_a_new_video_with_a_file()
    {
    	$response = $this->post('admins/gallery', ['title' => 'video title', 'type' => 'video', 'video' => UploadedFile::fake()->create('video.mp4', 15000)]);

        $video = Gallery::first();

    	$this->assertNotNull($video);
    	$this->assertSame('video title', $video->title);

    	Storage::fake();
    	Storage::disk('gallery')->assertExists($video->path);
    }

    /** @test */
    public function it_can_update_an_existing_youtube_link()
    {
    	$gallery = factory(Gallery::class)->states('youtube_url')->create();

    	$gallery->title = 'new title';
    	$gallery->youtube_url = 'https://www.youtube.com/new-youtube-link';

    	$response = $this->patch("admins/gallery/{$gallery->id}", ['title' => $gallery->title, 'youtube_url' => $gallery->youtube_url]);
    	$response->assertRedirect('admins/gallery');

    	$gallery_from_db = Gallery::first();
    	$this->assertSame($gallery->title, $gallery_from_db->title);
    	$this->assertSame($gallery->youtube_url, $gallery_from_db->youtube_url);
    }

    /** @test */
    public function it_deletes_youtube_gallery_entries()
    {
    	$gallery = factory(Gallery::class)->states('youtube_url')->create();

    	$response = $this->delete("admins/gallery/{$gallery->id}");

    	$response->assertRedirect('admins/gallery');
    	$this->assertEquals(0, Gallery::count());
    }
}
