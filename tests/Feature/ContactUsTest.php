<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Mail\ContactMessage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactUsTest extends TestCase
{
	public function setUp()
	{
		parent::setUp();

		$this->name = 'John Doe';
		$this->email = 'email@example.com';
		$this->subject = 'A warm thank you to all the guys behind this great work.';
		$this->message = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae, dolorum facilis repellat dolore nemo fuga atque corporis labore ducimus eligendi, voluptate, animi sunt saepe provident a cum reprehenderit ipsa in?';
	}

    /** @test */
    public function it_sends_an_email_with_form_inputs()
    {
    	$this->disableExceptionHandling();

    	Mail::fake();
    	
    	$response = $this->post(route('contact-us.store'), [
    		'name' => $this->name,
    		'email' => $this->email,
    		'subject' => $this->subject,
    		'message' => $this->message,
    	]);

    	Mail::assertQueued(ContactMessage::class, function($email){
    		return $email->inputs['name'] == $this->name;
    	});
    }

    /** @test */
    public function it_validates_required_inputs()
    {
    	$response = $this->post(route('contact-us.store'), []);

    	$response->assertSessionHasErrors(['name', 'email', 'subject', 'message']);
    }

    /** @test */
    public function it_validates_email_format()
    {
    	$response = $this->post(route('contact-us.store'), [
    		'name' => $this->name,
    		'email' => 'an-invalid-email',
    		'subject' => $this->subject,
    		'message' => $this->message,
    	]);

    	$response->assertSessionHasErrors('email');
    }
}
