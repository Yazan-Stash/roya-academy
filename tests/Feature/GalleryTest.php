<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Gallery;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GalleryTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function it_can_see_gallery_images_without_videos()
    {
    	$images = factory(Gallery::class, 5)->create(['path' => 'image.jpeg']);
    	$video = factory(Gallery::class)->create(['path' => 'video.mp4']);

    	$response = $this->get('gallery');

    	$images->each(function($image) use ($response) {
	    	$response->assertSee($image->title);
	    	$response->assertSee((string)$image->url);
    	});

    	$response->assertDontSee($video->title);
    }

   	/** @test */
	public function it_can_see_at_most_eight_gallery_images()
	{
		$images = factory(Gallery::class, 10)->states('image_url')->create();

		$response = $this->get('/gallery');

		$view_data = $response->getOriginalContent()->getData();

		$this->assertLessThanOrEqual(8, count($view_data['gallery']));
	}
}
