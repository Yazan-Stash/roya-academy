<?php

namespace Tests\Feature;

use App\Admin;
use App\Course;
use App\Attachment;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CourseAttachmentAdminTest extends TestCase
{
	use RefreshDatabase;

    public function setUp()
    {
        parent::setUp();

        $admin = factory(Admin::class)->create();
        $this->actingAs($admin, 'admin');
    }

	/** @test */
	public function sees_a_list_of_attachments_related_to_a_course()
	{
		$this->disableExceptionHandling();
		$course = factory(Course::class)->create();
		$attachments = factory(Attachment::class, 3)->create(['course_id' => $course->id]);

		$response = $this->get("admins/courses/{$course->id}/attachments");

		$attachments->each(function($attachment) use ($response){
			$response->assertSee($attachment->title);
		});	
	}

    /** @test */
    public function attaches_attachments_to_a_course()
    {
        $course = factory(Course::class)->create();

        $response = $this->post("admins/courses/{$course->id}/attachments", [
        	'title' => 'attachment_title',
        	'attachment' => UploadedFile::fake()->create('file.pdf')
        ]);

        $response->assertRedirect("admins/courses/{$course->id}/attachments");
        $this->assertCount(1, $course->attachments);
        $this->assertSame('attachment_title', $course->attachments->first()->title);
    }

    /** @test */
    public function deletes_attachments_related_to_a_course()
    {
    	$course = factory(Course::class)->create();
        $response = $this->post("admins/courses/{$course->id}/attachments", [
        	'title' => 'attachment_title',
        	'attachment' => UploadedFile::fake()->create('file.pdf')
        ]);
        $attachment = Attachment::first();
        $this->assertNotNull($attachment);
        $file_path = $attachment->path;

        $response = $this->delete("admins/courses/{$course->id}/attachments/{$attachment->id}");

        $response->assertRedirect("admins/courses/{$course->id}/attachments");
        $this->assertNull(Attachment::first());

        Storage::fake();
        Storage::assertMissing(storage_path('app/' . $file_path));
    }

    /** @test */
    public function validates_required_fields()
    {
    	$course = factory(Course::class)->create();
    	
    	$response = $this->post("admins/courses/{$course->id}/attachments", []);


    	$response->assertSessionHasErrors(['title', 'attachment']);
    }

    /** @test */
    public function validates_file_type_is_pdf()
    {
        $course = factory(Course::class)->create();
        
        $response = $this->post("admins/courses/{$course->id}/attachments", ['attachment' => UploadedFile::fake()->create('file.jpg')]);

        $response->assertSessionHasErrors(['attachment']);
    }
}
