<?php

namespace Tests;

use Exception;
use App\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function disableExceptionHandling()
    {
    	$this->app->instance(ExceptionHandler::class, new class extends Handler {
    		public function __construct() {}
    		public function report(Exception $e) {}
    		public function render($request, Exception $exception) {
                throw $exception;
            }
    	});
    }

    protected function disableImageUpload()
    {
        $this->app->instance(\App\ImageStore::class, new class {
            public function moveRequestImageTo($image){ return 'new/path';}
            public function delete($image){}
        });
    }

    public function dumpSession()
    {
        dd($this->app['session.store']);
    }

    public function getViewData($response, $key = null)
    {
        if (is_null($key)) {
            return $response->getOriginalContent()->getData();
        }

        return $response->getOriginalContent()->getData()[$key];
    }
}
