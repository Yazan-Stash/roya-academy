tinymce.PluginManager.add('extraArticle', function(editor, url) {
  // Add a button that opens a window
  editor.addButton('extraArticle', {
    text: 'اقرأ أيضاً',
    icon: false,
    onclick: function() {
      // Open window
      editor.windowManager.open({
        title: 'إضافة خبر آخر',
        body: [
          {type: 'textbox', name: 'read_more', label: 'عنوان الخبر'},
          {type: 'textbox', name: 'news_id', label: 'رمز الخبر'}
        ],
        onsubmit: function(e) {
          // Insert content when the window form is submitted
          editor.insertContent('<hr><p class="extra_title"><a href="http://royanews.tv/news/'+ e.data.news_id +'">اقرأ أيضاً : '+ e.data.read_more + '</a></p><hr>');
        }
      });
    }
  });

  // Adds a menu item to the tools menu
  editor.addMenuItem('extraArticle', {
    text: 'اقرأ أيضاً',
    context: 'tools',
    onclick: function() {
      // Open window with a specific url
      editor.windowManager.open({
        title: 'TinyMCE site',
        url: 'https://www.tinymce.com',
        width: 800,
        height: 600,
        buttons: [{
          text: 'إغلاق',
          onclick: 'close'
        }]
      });
    }
  });

  return {
    getMetadata: function () {
      return  {
        title: "اقرأ أيضاً",
        url: "http://exampleplugindocsurl.com"
      };
    }
  };
});

