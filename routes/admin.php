<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');

/** Staff **/
Route::prefix('staff')->as('staff.')->group(function () {
    Route::get('/', 'StaffController@index')->name('index');
    Route::get('/create', 'StaffController@create')->name('create');
    Route::post('/', 'StaffController@store')->name('store');
    Route::get('/{staff}/edit', 'StaffController@edit')->name('edit');
    Route::patch('/{staff}', 'StaffController@update')->name('update');
    Route::delete('/{staff}', 'StaffController@destroy')->name('delete');
});

/** Main Slider **/
Route::prefix('sliders')->as('sliders.')->group(function () {
    Route::get('/', 'SlidersController@index')->name('index');
    Route::get('create', 'SlidersController@create')->name('create');
    Route::post('/', 'SlidersController@store')->name('store');
    Route::get('{slider}/edit', 'SlidersController@edit')->name('edit');
    Route::patch('/{slider}', 'SlidersController@update')->name('update');
    Route::delete('/{slider}', 'SlidersController@destroy')->name('delete');
});

/** Gallery Images **/
Route::prefix('gallery')->as('gallery.')->group(function () {
    Route::get('/', 'GalleryController@index')->name('index');
    Route::get('create', 'GalleryController@create')->name('create');
    Route::post('/', 'GalleryController@store')->name('store');
    Route::get('{gallery}/edit', 'GalleryController@edit')->name('edit');
    Route::patch('/{gallery}', 'GalleryController@update')->name('update');
    Route::delete('/{gallery}', 'GalleryController@destroy')->name('delete');
});

/** Posts **/
Route::prefix('posts')->as('posts.')->group(function () {
    Route::get('/', 'PostsController@index')->name('index');
    Route::get('create', 'PostsController@create')->name('create');
    Route::post('/', 'PostsController@store')->name('store');
    Route::get('{post}/edit', 'PostsController@edit')->name('edit');
    Route::patch('/{post}', 'PostsController@update')->name('update');
    Route::delete('/{post}', 'PostsController@destroy')->name('delete');
});

/** Courses **/
Route::prefix('courses')->as('courses.')->group(function () {
    /** Introduction **/
    Route::prefix('intro')->as('intro.')->group(function(){
        Route::get('edit', 'CoursesIntroductionController@edit')->name('edit');
        Route::patch('/', 'CoursesIntroductionController@update')->name('update');
    });

    /** Diploma **/
    Route::prefix('diploma')->as('diploma.')->group(function(){
        Route::get('edit', 'CoursesDiplomaController@edit')->name('edit');
        Route::patch('/', 'CoursesDiplomaController@update')->name('update');
    });
    
    Route::get('/', 'CoursesController@index')->name('index');
    Route::get('create', 'CoursesController@create')->name('create');
    Route::post('/', 'CoursesController@store')->name('store');
    Route::get('{course}/edit', 'CoursesController@edit')->name('edit');
    Route::patch('/{course}', 'CoursesController@update')->name('update');
    Route::delete('/{course}', 'CoursesController@destroy')->name('delete');


    /** Attachments **/
    Route::prefix('{course}/attachments')->as('attachments.')->group(function () {
        Route::get('/', 'CourseAttachmentsController@index')->name('index');
        Route::post('/', 'CourseAttachmentsController@store')->name('store');
        Route::delete('/{attachment}', 'CourseAttachmentsController@destroy')->name('delete');
    });
});

/** Guests **/
Route::prefix('guests')->as('guests.')->group(function () {
    Route::get('/', 'GuestsController@index')->name('index');
    Route::get('create', 'GuestsController@create')->name('create');
    Route::post('/', 'GuestsController@store')->name('store');
    Route::get('{guest}/edit', 'GuestsController@edit')->name('edit');
    Route::patch('/{guest}', 'GuestsController@update')->name('update');
    Route::delete('/{guest}', 'GuestsController@destroy')->name('delete');
});

Route::namespace('Auth')->group(function(){
    // Authentication Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('login');
    $this->post('login', 'LoginController@login');
    $this->get('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
    $this->get('register', 'RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset');
});
