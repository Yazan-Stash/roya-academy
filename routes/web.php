<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomepageController@index')->name('homepage');
Route::get('/about-us', 'AboutPageController@index')->name('about-us');

Route::get('/gallery', 'GalleryController@index')->name('gallery.index');

Route::get('/contact-us', 'ContactUsController@create')->name('contact-us.create');
Route::post('/contact-us', 'ContactUsController@store')->name('contact-us.store');

Route::get('/posts', 'PostsController@index')->name('posts.index');
Route::get('/posts/{post}', 'PostsController@show')->name('posts.show');

Route::get('/staff', 'StaffController@index')->name('staff.index');
Route::get('/staff/{staff}', 'StaffController@show')->name('staff.show');

Route::get('/courses', 'CoursesController@index')->name('courses.index');
Route::get('/courses/intro', 'CoursesController@intro')->name('courses.intro');
Route::get('/courses/diploma', 'CoursesController@diploma')->name('courses.diploma');
Route::get('/courses/{course}', 'CoursesController@show')->name('courses.show');

Route::get('/newsletter/activate-email/{code}', 'NewsletterSubscribersController@activateEmail')->name('newsletter.activate-email');
