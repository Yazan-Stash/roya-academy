@extends('includes.master')
@section('title', 'الصفحة الرئيسية')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي')
@section('description','أكاديمية رؤيا للتدريب الإعلامي')

@section('content')
    @include ('homepage.slider')

    @include ('homepage.about')

    @include ('homepage.courses')

    @include ('homepage.staff')

    @include ('homepage.posts')
@endsection