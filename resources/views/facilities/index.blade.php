<div class="rs-history sec-spacer">
	<div class="container">
		<div class="abt-title">
			<h2>التجهيزات والمرافق</h2>
		</div>

		<!-- Facilities Images -->
		<div class="rs-gallery-4 rs-gallery mb-30">
			<div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="false"
				 data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true" data-nav-speed="false"
				 data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="true" data-ipad-device="2"
				 data-ipad-device-nav="true" data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true"
				 data-md-device-dots="true">

				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F1.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F1.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F2.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F2.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F3.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F3.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F4.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F4.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F5.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F5.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F6.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F6.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F7.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F7.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F8.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F8.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F9.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F9.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
				<div class="gallery-item">
					<img src="/theme/images/about/facilities/F10.JPG" alt="مرفق الأكاديمية" title="مرفق الأكاديمية">
					<div class="gallery-desc">
						<a class="image-popup" href="/theme/images/about/facilities/F10.JPG" title="مرفق الأكاديمية">
							<i class="fa fa-search"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<!-- End of Facilities Images -->

		<!-- Facilities Description -->
		<div class="row">
			<div class="col-lg-12 mobile-mb-50">
				<div class="about-desc">
					<p>تمتلك أكاديمية رؤيا للتدريب الإعلامي كافة مقومات التدريب وكافة التجهيزات التقنية الحديثة من غرف المحاضرات - الأحدث على مستوى الوطن العربي – ، واستوديو حقيقي مجهز بالكامل ، وغرفة كنترول كاملة المعدات ، وجاليري متكامل ، و وحدة مونتاج وتصميم بأعلى التقنيات، بالإضافة لكون جميع استديوهات و أجهزة و معدات و عربات النقل التابعة لقناة رؤيا الفضائية تحت تصرف الأكاديمية عند الحاجة.</p>
				</div>
			</div>
		</div>
		<!-- End of Facilities Description -->
	</div>
</div>