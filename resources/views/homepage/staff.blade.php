<!-- Team Start -->
@if(count($staff) > 0)
<div id="rs-team" class="rs-team sec-color sec-spacer">
    <div class="container">
        <div class="sec-title mb-50 text-center">
            <h2>كادر الأكاديمية</h2>
        </div>
        <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="false"
             data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true" data-nav-speed="false"
             data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="true" data-ipad-device="2"
             data-ipad-device-nav="true" data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true"
             data-md-device-dots="true">
            <!-- Team Members -->
            @foreach ($staff as $staff_member)
                <div class="team-item">
                    <div class="team-img">
                        <a href="{{ route('staff.show',['staff'=> $staff_member->id]) }}" title="{{ $staff_member->name }}">
                            <img src="{{ $staff_member->image_url }}" alt="{{ $staff_member->name }}" title="{{ $staff_member->name }}" />
                            <div class="normal-text">
                                <h3 class="team-name">{{ $staff_member->name }}</h3>
                                <span class="subtitle">{{ $staff_member->title }}</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endif
<!-- Team End -->
