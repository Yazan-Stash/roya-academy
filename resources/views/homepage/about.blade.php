<!-- About Us Start -->
<div id="rs-about" class="rs-about sec-spacer">
    <div class="container">
        <div class="sec-title mb-30 text-center">
            <h2>أكاديمية رؤيا</h2>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="gallery-video about-video-wrapper">
                    <video controls preload="none" width="100%" height="304px" poster="/theme/images/RALogo.png">
                        <source src="/theme/images/about/RoyaAcademy.mp4" type="video/mp4">
                        <p>Your browser does not support HTML5 video.</p>
                    </video>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="about-desc">
                    <h3>أكاديمية رؤيا للتدريب الإعلامي</h3>
                    <p>أسست أكاديمية رؤيا للتدريب الإعلامي من قبل قناة رؤيا الفضائية في عام 2018 لملء الفراغ الكبير في صناعة التدريب الإعلامي من خلال الريادة في خلق نهج تدريب إعلامي متكامل ومتعدد الأبعاد.</p>
                </div>
                <div id="accordion" class="rs-accordion-style1">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h3 class="acdn-title" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">نبذة عن الأكاديمية</h3>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">تقدم  أكاديمية رؤيا للتدريب الإعلامي العديد من البرامج التدريبية الإحترافية من خلال مركز تدريبي و تعليمي متطور و مجهز بالكامل ، بحيث  تجذب الموهوبين و الفنانين و الإعلاميين من كل أنحاء العالم لتنمية مهاراتهم الفنية و الإعلامية.</div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h3 class="acdn-title collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                aria-expanded="false" aria-controls="collapseTwo">الشهادات</h3>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">تمنح أكاديمية رؤيا للتدريب الإعلامي أنواع عديدة من الدرجات الاحترافية و الشهادات الصادرة من الأكاديمية العالمية للفنون و الاعلام و الابداع
                                <span style="direction:ltr;display:block">International Academy of Arts, Media, and Creativity IAAMC.</span>
                                و هي إحدى أهم الأقطاب الأمريكية لإعتماد الدراسات الاحترافية للفنون و الإعلام  ضمن المجموعة الأمريكية لإدارة المشاريع التعليمية.
                                <span style="direction:ltr;display:block">American Group for Managing Educational Projects (AGMEP)</span></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header mb-0" id="headingThree">
                            <h3 class="acdn-title collapsed" data-toggle="collapse" data-target="#collapseThree"
                                aria-expanded="false" aria-controls="collapseThree">اختر أكاديمية رؤيا</h3>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">أكاديمية رؤيا للتدريب الإعلامي تابعة لقناة رؤيا الفضائية ، و ستوفر تدريب و خبرات عملية فعلية على منصات ، و استوديوهات ، و معدات ، و برامج القناة ، كما أن الأكاديمية بمدربيها و خبرائها وشركائها تمتلك سنوات عديدة من الخبرة المشتركة  في مجالات الإعلام والصحافة والتلفزيون والإبداع والتدريب.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Us End -->
@section('scripts')
    <script src="/theme/js/about-academy.js"></script>
@endsection