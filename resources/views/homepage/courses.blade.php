<!-- Courses Start -->
@if(count($courses) > 0)
<div id="rs-courses" class="rs-courses sec-color sec-spacer">
    <div class="container">
        <div class="sec-title mb-50 text-center">
            <h2>دوراتنا</h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="false"
                     data-autoplay-timeout="5000" data-smart-speed="1200" data-dots="true" data-nav="true" data-nav-speed="false"
                     data-mobile-device="1" data-mobile-device-nav="true" data-mobile-device-dots="true" data-ipad-device="2"
                     data-ipad-device-nav="true" data-ipad-device-dots="true" data-md-device="3" data-md-device-nav="true"
                     data-md-device-dots="true">
                   @foreach ($courses as $course)
                        <div class="cource-item">
                            <div class="cource-img">
                                <a href="{{ route('courses.show', [$course->id]) }}" title="{{ $course->title }}" class="course_link">
                                    <img src="{{ $course->image_url }}" alt="{{ $course->title }}" title="{{ $course->title }}" />
                                </a>
                            </div>
                            <div class="course-body">
                                <a href="#" class="course-category">{{ $course->category }}</a>
                                <h4 class="course-title">
                                    <a href="{{ route('courses.show', [$course->id]) }}">{{ str_limit($course->title,50) }}</a>
                                </h4>
                                <div class="course-desc">
                                    <a href="{{ route('courses.show', [$course->id]) }}">
                                        {!! $course->excerpt(64) !!}
                                    </a>
                                </div>
                            </div>
                            <div class="course-footer">
                                <div class="course-seats">
                                    <i class="fa fa-info-circle"></i> تنعقد: {{ $course->status }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<!-- Courses End -->
