@if ($posts->count() > 0)
<!-- Latest News Start -->
<div id="rs-latest-news" class="rs-latest-news sec-spacer">
    <div class="container">
        <div class="sec-title mb-50 text-center">
            <h2>أخبارنا وفعالياتنا</h2>
        </div>
        <div class="row">
            <!-- Latest Post -->
                <div class="col-md-6">
                    <div class="news-normal-block">
                        <div class="news-img">
                            <a href="{{ $posts->first()->url() }}">
                                <img src="{{ $posts->first()->image_url }}" alt="{{ $posts->first()->title }}" title="{{ $posts->first()->title }}" />
                            </a>
                        </div>
                        <div class="news-date">
                            <i class="fa fa-calendar-check-o"></i>
                            <span>{{ $posts->first()->formatted_created_at }}</span>
                        </div>
                        <h4 class="news-title">
                            <a href="{{ $posts->first()->url() }}">{{ str_limit($posts->first()->title,100) }}</a>
                        </h4>
                        <div class="news-desc">
                            {!! $posts->first()->excerpt(140) !!}
                        </div>
                    </div>
                </div>

            <!-- Posts -->
            <div class="col-md-6">
                <div class="news-list-block">
                    @foreach ($posts as $post)
                        @continue($loop->first)
                        <div class="news-list-item">
                            <div class="news-img">
                                <a href="{{ $post->url() }}">
                                    <img src="{{ $post->image_url }}" alt="{{ $post->title }}" title="{{ $post->title }}" />
                                </a>
                            </div>
                            <div class="news-content">
                                <h5 class="news-title">
                                    <a href="{{ $post->url() }}">{{ str_limit($post->title,34) }}</a>
                                </h5>
                                <div class="news-date">
                                    <i class="fa fa-calendar-check-o"></i>
                                    <span>{{ $post->formatted_created_at }}</span>
                                </div>
                                <div class="news-desc d-none d-lg-block">
                                    {!! $post->excerpt(70) !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Latest News End -->
@endif
