<!-- Slider Area Start -->
@if(count($sliders) >= 2)
<div id="rs-slider" class="slider-overlay-2">
    <div id="home-slider">
        <!-- Slider Items -->
        @foreach ($sliders as $slider)
            <a href="{{ $slider->link }}" title="{{ $slider->heading }}">
                <div class="item active">
                    <img src="{{ $slider->image_url }}" alt="{{ $slider->heading }}" title="{{ $slider->heading }}" />
                </div>
            </a>
        @endforeach
    </div>
</div>
@endif
<!-- Slider Area End -->
