@component('mail::message')
### رسالة عبر الموقع

@component('mail::table')
|							|								|
| ------------------------- | ----------------------------: |
| التاريخ      				| {{ date('H:i d/m/Y') }}    		| 
| الإسم      				| {{ $inputs['name'] }}    		| 
| البريد الإلكتروني      	| {{ $inputs['email'] }}      	|
| موضوع الرسالة      		| {{ $inputs['subject'] }}      | 
| الرسالة      				| {{ $inputs['message'] }} 		|
@endcomponent

{{ config('app.name') }}
@endcomponent
