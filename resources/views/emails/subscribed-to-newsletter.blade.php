@component('mail::message')
# شكراً لك للإشتراك بالنشرة الإخبارية لأكاديمية رؤيا

لتأكيد اشتراكك يرجى الضغط على الزر في الأسفل

@component('mail::button', ['url' => route('newsletter.activate-email', ['code' => $subscriber->activation_code])])
تأكيد اشتراكي	
@endcomponent

إذا كنت لم تقم بهذه العملية، يمكنك إهمال هذه الرسالة

شكراً لك،<br>
{{ config('app.name') }}
@endcomponent
