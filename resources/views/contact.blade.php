@extends('includes.master')
@section('title', 'تواصل معنا')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، تواصل معنا، أرسل لنا')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">
		<div class="contact-page-section sec-spacer">
        	<div class="container">
				<!-- Location Map -->
	        		<div id="googleMap"></div>
				<!-- Location Map End -->

				<!-- Contact Information -->
        		<div class="row contact-address-section">
    				<div class="col-md-4 pl-0">
    					<div class="contact-info contact-address">
    						<i class="fa fa-map-marker"></i>
    						<h4>الموقع</h4>
    						<p>الأردن – عمان – شارع مكة</p>
    						<p>عمارة البنك التجاري الأردني (رقم 181) – ط 7</p>
    					</div>
    				</div>
    				<div class="col-md-4">
    					<div class="contact-info contact-phone">
    						<i class="fa fa-phone"></i>
    						<h4>رقم الهاتف</h4>
    						<a href="tel:+962 6 5542509">+962 6 5542509</a>
    						<a href="tel:+962 77 5775410">+962 77 5775410</a>
    					</div>
    				</div>
    				<div class="col-md-4 pr-0">
    					<div class="contact-info contact-email">
    						<i class="fa fa-envelope"></i>
    						<h4>البريد الإلكتروني</h4>
    						<a href="mailto:academy@roya.tv">academy@roya.tv</a>
        				</div>
        			</div>
        		</div>
				<!-- Contact Information End -->

        		<!-- Contact Us Form -->
				<div class="contact-comment-section">
        			<h3>تواصل معنا</h3>
					@if (count($errors) > 0)
						<div class="error-box">
							<ul>
								<i class="fa fa-warning mb_10"></i>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					@if (Session::has('success'))
						<div class="message-box">
							<i class="fa fa-check-square mb_10"></i>
							<p>{{ Session::get('success') }}</p>
						</div>
					@endif

					<form id="contact-form" method="post" action="{{ route('contact-us.store') }}">
						{{ csrf_field() }}
						<fieldset>
							<div class="row">                                      
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label>الإسم *</label>
										<input name="name" id="name" class="form-control" type="text" placeholder="قم بإدخال الإسم الكامل">
									</div>
								</div>
								<div class="col-md-6 col-sm-12">
									<div class="form-group">
										<label>البريد الإلكتروني *</label>
										<input name="email" id="email" class="form-control" type="email" placeholder="قم بإدخال بريدك الإلكتروني">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<label>العنوان *</label>
										<input name="subject" id="subject" class="form-control" type="text" placeholder="قم بإدخال عنوان الرسالة">
									</div>
								</div>
							</div>
							<div class="row"> 
								<div class="col-md-12 col-sm-12">    
									<div class="form-group">
										<label>الرسالة *</label>
										<textarea cols="40" rows="10" id="message" name="message" class="textarea form-control" placeholder="قم بتعبئة محتوى الرسالة"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group mb-0">
										<input class="btn-send" type="submit" value="أرسل لنا">
									</div>
								</div>
							</div>
						</fieldset>
					</form>						
        		</div>
				<!-- Contact Us Form End -->
        	</div>
        </div>
    </div>
@endsection
