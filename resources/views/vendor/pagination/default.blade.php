@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><a class="page-link fa fa-angle-right" href="#" tabindex="-1"></a></li>
        @else
            <li class="page-item"><a class="page-link fa fa-angle-right" href="{{ $paginator->previousPageUrl() }}" tabindex="-1" rel="prev"></a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><a class="page-link dotted" href="#">{{ $element }}</a></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item"><a class="page-link active" href="#">{{ $page }}</a></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="page-link fa fa-angle-left" href="{{ $paginator->nextPageUrl() }}" rel="next"></a></li>
        @else
            <li class="page-item disabled"><a class="page-link fa fa-angle-left" href="#"></a></li>
        @endif
    </ul>
@endif
