@extends('includes.master')
@section('title', $post->title)
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، أخبار، أحداث، فعاليات')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">
		<div class="single-blog-details sec-spacer">
			<div class="container">
				<div class="row">
					<!-- Post Block Start -->
					<div class="col-lg-8 col-md-12">
						<!-- Post Title -->
						<h1 class="top-title">{{ $post->title }}</h1>
						<!-- Post Title End -->

						<!-- Post Image -->
						<div class="single-image">
							<img src="{{ $post->image_url }}" alt="{{ $post->title }}" title="{{ $post->title }}">
						</div>
						<!-- Post Image End -->

						<!-- Publish Date -->
						<div class="date mb-15">
							<i class="fa fa-calendar" aria-hidden="true"></i> نشر: {{ $post->formatted_created_at }}
						</div>
						<!-- Publish Date End -->

						<!-- Post Body -->
						<div class="desc-text">
							{!! $post->body !!}
						</div>
						<!-- Post Body End -->

						{{-- <!-- Post Tags Block -->
						<div class="share-section">
							<div class="row">
								<div class="col-lg-12">
									<ul class="share-link1">
										<li><a href="#"> الأوسمة:</a></li>
										<li><a href="#"> Building</a></li>
										<li><a href="#"> Plumbing</a></li>
										<li><a href="#"> Painting</a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- Post Tags Block End --> --}}

						<!-- Post Sharing Block -->
						{{-- <div class="share-section2">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12">
									<div class="share-label"> قم بمشاركة الخبر: </div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12">
									<ul class="share-link">
										<li><a href="#"> <i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a></li>
										<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a></li>
										<li><a href="#"><i class="fa fa-google" aria-hidden="true"></i> Google</a></li>
									</ul>
								</div>
							</div>
						</div> --}}
						<!-- Post Sharing Block End -->
					</div>
					<!-- Post Block End -->

					<!-- Left Side -->
					<div class="col-lg-4 col-md-12">
						<div class="sidebar-area">
							@include ('includes.latest-courses')
						</div>
					</div>
					<!-- Left Side -->
				</div>
			</div>
		</div>
    </div>
@endsection
