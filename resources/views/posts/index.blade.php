@extends('includes.master')
@section('title', 'لائحة الأخبار والفعاليّات')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، أخبار، أحداث، نشاطات، فعاليّات')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">
        <!-- Posts Section Start Here -->
		<div class="blog-page-area sec-spacer">
			<div class="container">
                <!-- Posts -->
                <div class="row">
                    @if(count($posts) > 0)
                    @foreach ($posts as $post)
                        <div class="col-lg-6 col-md-12">
                            <div class="row mb-30 blog-inner">
                                <div class="col-lg-6 col-md-12">
                                    <a href="{{ $post->url() }}" title="{{ $post->title }}">
                                        <div class="blog-images">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                            <img src="{{ $post->image_url }}" alt="{{ $post->title }}" title="{{ $post->title }}">
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="blog-content">
                                        <div class="date">
                                            <i class="fa fa-calendar-check-o"></i> {{ $post->formatted_created_at }}
                                        </div>
                                        <h4>
                                            <a href="{{ $post->url() }}" title="{{ $post->title }}">{{ str_limit($post->title,50) }}</a>
                                        </h4>
                                        {!! $post->excerpt(50) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    @else
                        <div class="col-lg-12">
                            <p>سنوافيكم بأحدث أخبارنا وفعالياتنا قريباً</p>
                        </div>
                    @endif
                </div>

                {{ $posts->links() }}
			</div>
		</div>
        <!-- Posts End  -->
    </div>
@endsection
