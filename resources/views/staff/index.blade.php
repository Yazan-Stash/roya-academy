@extends('includes.master')
@section('title', 'كادر الأكاديمية')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، الكادر المهني، الطاقم التدريسي')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">
		<!-- Team Start -->
        <div id="rs-team-2" class="rs-team-2 pt-50 pb-70">
			<div class="container">
				<div class="row">

					@foreach ($staff as $staff_member)
						<div class="col-lg-3 col-md-6 col-xs-6">
			                <div class="team-item">
			                    <div class="team-img">
									<a href="{{ route('staff.show',['staff'=> $staff_member->id]) }}" title="{{ $staff_member->name }}">
			                        	<img src="{{ $staff_member->image_url }}" alt="{{ $staff_member->name }}" title="{{ $staff_member->name }}" />
									</a>
			                    </div>
			                    <div class="team-body">
	                                <a href="{{ route('staff.show',['staff'=> $staff_member->id]) }}">
										<h3 class="name">{{ $staff_member->name }}</h3>
									</a>
			                    	<span class="designation">{{ $staff_member->title }}</span>
			                    </div>
			                </div>
						</div>
	                @endforeach

	                {{ $staff->links() }}

			    </div>
			</div>
        </div>
        <!-- Team End -->
    </div>
@endsection
