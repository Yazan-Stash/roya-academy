@extends('includes.master')
@section('title', $staff->name)
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، الكادر المهني، الطاقم التدريسي')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">

		<!-- Team Single Start -->
		<div class="rs-team-single pt-50">
			<div class="container">
				<div class="row team">
				    <div class="col-lg-3 col-md-12">
				        <div class="team-photo mb-50 mobile-mb-40">
				            <img src="{{ $staff->image_url }}" alt="{{ $staff->name }}" title="{{ $staff->name }}">
				        </div>
				    </div>
				    <div class="col-lg-9 col-md-12">
				        <h3 class="team-name">{{ $staff->name }}</h3>
						<h5>{{ $staff->title }}</h5>
				        <p class="team-contact">
							@isset ($staff->phone)
				        	<i class="fa fa-mobile"></i> {{ $staff->phone }}
							@endisset
							@isset($staff->email)
							<i class="mr-25 fa fa-envelope-o"></i> <a href="mailTo:{{ $staff->email }}">{{ $staff->email }}</a>
							@endisset
				        </p>
						<div class="desc-text">
				        	{!! $staff->bio !!}
						</div>
				    </div>
				</div>
			</div>
		</div>
		<!-- Team Single End -->
    </div>
@endsection
