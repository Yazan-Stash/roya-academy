<div class="course-content">
    <div class="course-instructor">
        <div class="row">
            <div class="col-lg-12 mobile-mb-20">
                <h3 class="instructor-title"><span class="primary-color"> الضيف/الضيوف </span></h3>
                <div class="row">
                    @foreach($course->guests as $guest)
                    <div class="instructor-inner col-lg-6 col-xs-12 mb-15">
                        <div class="instructor-img">
                            <img src="{{ $guest->image_url }}" alt="{{ $guest->name }}" title="{{ $guest->name }}" />
                        </div>
                        <div class="instructor-body">
                            <a href="#" data-toggle="modal" data-target="#guestFullBio{{ $guest->id }}">
                                <h3 class="name">{{ $guest->name }}</h3>
                            </a>
                            <div class="designation">
                                @isset($guest->bio)
                                <div id="guestFullBio{{ $guest->id }}" class="guestFullBio modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body desc-text">
                                                <h4>{{ $guest->name }}</h4>
                                                {!! $guest->bio !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endisset
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>