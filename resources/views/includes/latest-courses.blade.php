<!-- Latest Courses Start -->
@if(count($latest_courses) > 0)
<div class="latest-courses">
	<h3 class="title">أحدث الدورات</h3>

	@foreach ($latest_courses as $latest_course)
		<div class="post-item">
			<div class="post-img">
				<a href="{{ route('courses.show', [$latest_course->id]) }}" title="{{ $latest_course->title }}">
					<img src="{{ $latest_course->image_url }}" alt="{{ $latest_course->title }}" title="{{ $latest_course->title }}">
				</a>
			</div>
			<div class="post-desc">
				<h4>
					<a href="{{ route('courses.show', [$latest_course->id]) }}" title="{{ $latest_course->title }}">
						{{ str_limit($latest_course->title,30) }}
					</a>
				</h4>
				@isset($latest_course->duration)
				<div class="duration">
					<i class="fa fa-clock-o" aria-hidden="true"></i> {{ $latest_course->duration }}
				</div>
				@endisset
				@isset($latest_course->price)
				<div class="price">السعر: <span>{{ $latest_course->price }} JD</span></div>
				@endisset
			</div>
		</div>
	@endforeach

</div>
@endif
<!-- Latest Course End -->
