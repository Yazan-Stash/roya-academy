<!-- our js script -->
<script src="/js/app.js"></script>
<script src="/theme/js/modernizr-2.8.3.min.js"></script>
<script src="/theme/js/jquery.min.js"></script>
<script src="/theme/js/bootstrap.min.js"></script>
<script src="/theme/js/owl.carousel.min.js"></script>
<script src="/theme/js/slick.min.js"></script>
<script src="/theme/js/isotope.pkgd.min.js"></script>
<script src="/theme/js/imagesloaded.pkgd.min.js"></script>
<script src="/theme/js/wow.min.js"></script>
<script src="/theme/js/waypoints.min.js"></script>
<script src="/theme/js/jquery.counterup.min.js"></script>
<script src="/theme/js/jquery.magnific-popup.min.js"></script>
<script src="/theme/js/rsmenu-main.js"></script>
<script src="/theme/js/plugins.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAgC6ZapXdUzFdeQOFhdm_wucwlDMMQ8CQ"></script>
<script src="/theme/js/main.js"></script>
