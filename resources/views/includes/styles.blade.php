<link rel="stylesheet" type="text/css" href="/theme/style.css">
<link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
<link rel="stylesheet" type="text/css" href="{{ mix('/css/fonts.css') }}">
<link rel="stylesheet" type="text/css" href="/theme/css/font-awesome.min.css">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->