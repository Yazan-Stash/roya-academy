<!-- Footer Start -->
<footer id="rs-footer" class="bg3 rs-footer">
    <div class="container">
        <!-- Footer Address -->
        <div class="row footer-contact-desc">
            <div class="col-md-4">
                <div class="contact-inner">
                    <i class="fa fa-map-marker"></i>
                    <h4 class="contact-title">الموقع</h4>
                    <p class="contact-desc">
                        الأردن – عمان – شارع مكة<br>
                        عمارة البنك التجاري الأردني (رقم 181) – ط 7
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-inner">
                    <i class="fa fa-phone"></i>
                    <h4 class="contact-title">رقم الهاتف</h4>
                    <p class="contact-desc">
                        <a href="tel:+962 6 5542509">+962 6 5542509</a><br/>
                        <a href="tel:+962 77 5775410">+962 77 5775410</a>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-inner">
                    <i class="fa fa-envelope-open"></i>
                    <h4 class="contact-title">البريد الإلكتروني</h4>
                    <p class="contact-desc">
                        <a href="mailto:academy@roya.tv">academy@roya.tv</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="about-widget">
                        <h5 class="footer-title">أكاديمية رؤيا</h5>
                        <p>أسست أكاديمية رؤيا للتدريب الإعلامي من قبل قناة رؤيا الفضائية في عام 2018 لملء الفراغ الكبير في صناعة التدريب الإعلامي من خلال الريادة في خلق نهج تدريب إعلامي متكامل ومتعدد الأبعاد.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <h5 class="footer-title">النشرة الإخبارية</h5>
                    <p>قم بالإشتراك في نشرتنا الإخبارية للحصول على آخر التحديثات والدورات</p>
                    <newsletter-subscribe></newsletter-subscribe>
                </div>
                <div class="col-lg-4 col-md-12">
                    <h5 class="footer-title">تابعونا</h5>
                    <div class="footer-share">
                        <ul>
                            <li>
                                <a href="http://facebook.com/AcademyRoya" target="_blank" title="أكاديمية رؤيا فايسبوك">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/AcademyRoya" target="_blank" title="أكاديمية رؤيا تويتر">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://instagram.com/AcademyRoya" target="_blank" title="أكاديمية رؤيا انستغرام">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/academyroya/" target="_blank" title="أكاديمية رؤيا لينكدان">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="copyright">
                <p>جميع الحقوق محفوظة © {{ date('Y') }}</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->
