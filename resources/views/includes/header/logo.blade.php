<!-- Logo Block -->
<div class="rs-header-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="logo-area text-center">
                    <a href="{{ route('homepage') }}" title="أكاديمية رؤيا للتدريب الإعلامي">
                        <img src="/theme/images/RALogo.png" alt="أكاديمية رؤيا للتدريب الإعلامي" title="أكاديمية رؤيا للتدريب الإعلامي">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Logo Block End -->