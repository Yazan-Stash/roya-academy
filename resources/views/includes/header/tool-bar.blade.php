<!-- Toolbar Start -->
<div class="rs-toolbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="rs-toolbar-right">
                    <div class="toolbar-share-icon">
                        <ul>
                            <li>
                                <a href="http://facebook.com/AcademyRoya" target="_blank" title="أكاديمية رؤيا فايسبوك">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/AcademyRoya" target="_blank" title="أكاديمية رؤيا تويتير">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://instagram.com/AcademyRoya" target="_blank" title="أكاديمية رؤيا انستغرام">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/academyroya/" target="_blank" title="أكاديمية رؤيا لينكدان">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Toolbar End -->