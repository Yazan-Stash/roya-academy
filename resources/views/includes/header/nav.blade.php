<!-- Menu Start -->
<div class="menu-area menu-sticky">
    <div class="container">
        <div class="main-menu">
            <div class="row">
                <div class="col-sm-12">
                    <a class="rs-menu-toggle"><i class="fa fa-bars"></i>القائمة الرئيسية</a>
                    <!-- Main Menu -->
                    <nav class="rs-menu">
                        <ul class="nav-menu">
                            <!-- Home -->
                            <li class="current-menu-item current_page_item home">
                                <a href="{{ route('homepage') }}" class="home nav-link">الرئيسية</a>
                            </li>
                            <!-- End Home -->

                            <!--About -->
                            <li><a href="{{ route('about-us') }}" class="nav-link">عن الأكاديمية</a></li>
                            <!--About End -->

                            <!--Staff -->
                            <li><a href="{{ route('staff.index') }}" class="nav-link">كادر الأكاديمية</a></li>
                            <!--Staff End -->

                            <!--Courses Menu Start-->
                            <li class="menu-item-has-children">
                                <a href="#">الدورات</a>
                                <ul class="sub-menu">
                                    <li><a href="{{ route('courses.intro') }}" class="nav-link">عن الدورات</a></li>
                                    <li><a href="{{ route('courses.diploma') }}" class="nav-link">عن برامج الدبلوم الإحترافي</a></li>
                                    <li><a href="{{ route('courses.index') }}" class="nav-link">دوراتنا</a></li>
                                </ul>
                            </li>
                            <!--Courses Menu End-->

                            <!--Media Menu Start-->
                            <li class="menu-item-has-children">
                                <a href="#">المركز الإعلامي</a>
                                <ul class="sub-menu">
                                    <li><a href="{{ route('gallery.index') }}" class="nav-link">معرض الصور والفيديوهات</a></li>
                                    <li><a href="{{ route('posts.index') }}" class="nav-link">الأخبار والفعاليات</a></li>
                                </ul>
                            </li>
                            <!--Media Menu End-->

                            <!--Contact Menu Start-->
                            <li><a href="{{ route('contact-us.create') }}" class="nav-link">تواصل معنا</a></li>
                            <!--Contact Menu End-->
                        </ul>
                    </nav>
                    <!-- End of Main Menu -->
                    <!-- Search Icon -->
                    <?php
                            /*
                    <div class="right-bar-icon rs-offcanvas-link text-left">
                        <a class="hidden-xs rs-search" data-target=".search-modal" data-toggle="modal" href="#">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                            */
                            ?>
                    <!-- End of Search Icon -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Menu End -->

<!-- Search Modal Start -->
<?php
/*
@include('includes.header.search')
*/
?>
<!-- Search Modal End -->
@section ('scripts')
    
    @parent
    
    <script>
        $(document).ready(function() {
            var pathname = window.location.href;
            var app_url = '{{ config('app.url') }}';

            if (pathname.substring(0, pathname.length - 1) !== app_url) {
                $('li.home').removeClass('current-menu-item current_page_item');
                $('a[href="'+pathname+'"].nav-link').parent().addClass('current-menu-item current_page_item');
                $('a[href="'+pathname+'"].nav-link').parents('li').addClass('current-menu-item current_page_item');
            }
        });
    </script>
@endsection
