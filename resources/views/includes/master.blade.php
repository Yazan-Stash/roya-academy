<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="UTF-8" http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('includes.styles')
    <meta name="keyword" content="@yield('keyword')"/>
    <meta name="description" content="@yield('description')"/>
    <title>أكاديمية رؤيا  للتدريب الإعلامي | @yield('title')</title>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122379636-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-122379636-1');
    </script>
    
    @yield('head')
</head>

<body>
    <!-- Full Header Block -->
    <div class="home1">
        <div class="full-width-header">
        @include ('includes.header.tool-bar')
        <!-- Header Start -->
            <header id="rs-header" class="rs-header">
                @include ('includes.header.logo')
                @include ('includes.header.nav')
            </header>
            <!-- Header End -->
        </div>
        <!--Full Header Block End-->
    </div>
    
    <div id="app">
        <!-- Content -->
        @yield('content')
        <!-- End of Content -->

        <!-- Footer -->
        @include ('includes.footer')
        <!-- End of Footer -->
    </div>

    <!-- start ScrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End of ScrollUp -->
</body>

@include ('includes.scripts')
@yield('scripts')
</html>
