@extends('includes.master')
@section('title', 'تأكيد إشتراك في النشرة الإخبارية')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي')
@section('description','أكاديمية رؤيا للتدريب الإعلامي')

@section('content')
	<div class="container sec-spacer">
		<div class="row">
			<div class="col-xs-12">
				<p>
					لقد تم تأكيد إشتراكك بالنشرة الإخبارية لأكاديمية رؤيا للتدريب الإعلامي
				</p>
				<p>
					البريد المفعّل: {{ $email }}
				</p>
			</div>
		</div>
	</div>
@endsection
