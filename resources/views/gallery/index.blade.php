@extends('includes.master')
@section('title', 'معرض الصور')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، معرض، صور')
@section('description','أكاديمية رؤيا للتدريب الإعلامي')

@section('content')
    <!-- Gallery Start -->
    <div class="rs-gallery-4 rs-gallery sec-spacer">
        <div class="container">
            <div class="sec-title-2 mb-30 text-center">
                <h2>معرض الصور والفيديوهات</h2>
            </div>
            <div class="row gallery">
                @foreach ($gallery as $gallery_item)
                    <div class="col-lg-6 col-md-6 rs-about">
                        <!-- Gallery Image -->
                        @if($gallery_item->isImage())
                        <div class="gallery-item">
                            <img src="{{ $gallery_item->asset_url }}" alt="{{ $gallery_item->title }}" title="{{ $gallery_item->title }}" />
                            <div class="gallery-desc">
                                <h3>{{ $gallery_item->title }}</h3>
                                <a class="image-popup" href="{{ $gallery_item->asset_url }}" title="{{ $gallery_item->title }}">
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                        </div>
                        <!-- End of Gallery Image -->

                        <!-- Gallery Youtube -->
                        @elseif($gallery_item->isYoutube())
                        <div class="gallery-video">
                            <iframe src="{{ ($gallery_item->youtube_url) }}" frameborder="0" allowfullscreen
                                    width="100%" height="304px"></iframe>
                        </div>
                        <!-- End of Gallery Youtube -->

                        <!-- Gallery Video -->
                        @elseif($gallery_item->isNotYoutube())
                        <div class="gallery-video">
                            <video controls preload="auto" width="100%" height="304px">
                                <source src="{{ $gallery_item->asset_url }}" type="video/mp4">
                                <p>Your browser does not support HTML5 video.</p>
                            </video>
                        </div>
                        <!-- End if Gallery Video -->
                        @endif
                    </div>
                @endforeach
            </div>
            {{ $gallery->links() }}
        </div>
    </div>
    <!-- Gallery End -->
@endsection