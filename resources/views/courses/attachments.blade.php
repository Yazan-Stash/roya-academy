<div class="course-features-info">
    <h4 class="desc-title">الملفّات المرفقة للدورة</h4>
    <ul>
        @foreach($course->attachments as $attachment)
        <li>
            <i class="fa fa-file-pdf-o"></i>
            <a href="{{ $attachment->download_url }}" title="{{ $attachment->title }}" download>
                <span class="label">{{ $attachment->title }}</span>
            </a>
        </li>
        @endforeach
    </ul>
</div>
