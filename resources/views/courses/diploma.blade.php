@extends('includes.master')
@section('title', ' عن برامج الدبلوم الإحترافي')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، دورات إعلاميّة')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">
        <div class="rs-courses-details pt-50 pb-70">
            <div class="container">
                <div class="row mb-30">
                    <div class="col-lg-12 intro desc-text">
                        {!! $diploma->diploma !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection