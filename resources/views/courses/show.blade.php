@extends('includes.master')
@section('title', $course->title)
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، دورة تدريبية')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
    <div class="inner-page">
        <!-- Courses Details Start -->
        <div class="rs-courses-details pt-50 pb-70">
            <div class="container">
                <div class="row mb-30">
                    <!-- Right Side -->
                    <div class="col-lg-8 col-md-12">
                        <!-- Course Image -->
                        <div class="detail-img">
                            <img src="{{ $course->image_url }}" alt="{{ $course->title }}" title="{{ $course->title }}" />
                        </div>
                        <!-- Course Image End -->

                        <div class="course-desc mb-30">
                            <h2>{{ $course->title }}</h2>
                        </div>

                        <!-- Course Instructors -->
                        @if(count($course->instructors) > 0)
                        <div class="course-content">
                            <div class="course-instructor">
                                <div class="row">
                                    <div class="col-lg-12 mobile-mb-20">
                                        <h3 class="instructor-title"><span class="primary-color">المُحاضر/ين</span></h3>
                                        <div class="row">                                            
                                            @foreach ($course->instructors as $instructor)
                                                <div class="instructor-inner col-lg-6 col-xs-12 mb-15">
                                                    <a href="{{ route('staff.show',['staff' => $instructor->id]) }}" title="{{ $instructor->name }}">
                                                        <div class="instructor-img">
                                                            <img src="{{ $instructor->image_url }}" alt="{{ $instructor->name }}" title="{{ $instructor->name }}" />
                                                        </div>
                                                    </a>
                                                    <div class="instructor-body">
                                                        <a href="{{ route('staff.show',['staff' => $instructor->id]) }}" title="{{ $instructor->name }}">
                                                            <h3 class="name">{{ $instructor->name }}</h3>
                                                        </a>
                                                        <span class="designation">{{ $instructor->title }}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- Course Instructors End -->

                        <!-- Course Guests -->
                        @if(count($course->guests) > 0)
                            @include('includes.guests')
                        @endif
                        <!-- Course Guests End -->

                        <!-- Course Description -->
                        <div class="course-desc mb-30">
                            <div class="desc-text">
                                {!! $course->description !!}
                            </div>
                        </div>
                        <!-- Course Description End -->

                    </div>
                    <!-- Right Side -->

                    <!-- Left Side -->
                    <div class="col-lg-4 col-md-12">
                        <div class="sidebar-area">
                            <!-- Course Features -->
                            @include('courses.specs')
                            <!-- Course Features End -->

                            <!-- Course Features -->
                            @if(count($course->attachments) > 0)
                                @include('courses.attachments')
                            @endif
                            <!-- Course Features End -->

                            <!-- Latest Courses -->
                            @include ('includes.latest-courses')
                            <!-- Latest Courses End -->
                        </div>
                    </div>
                    <!-- Left Side -->
                </div>
            </div>
        </div>
        <!-- Courses Details End -->
    </div>
@endsection