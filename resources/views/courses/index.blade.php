@extends('includes.master')
@section('title', ' لائحة الدورات')
@section('keyword','رؤيا , أكاديمية، تدريب، إعلامي، دورات إعلاميّة')
@section('description','أكاديمية رؤيا  للتدريب الإعلامي')

@section('content')
<div class="inner-page">
    <!-- Courses Start -->
    <div id="rs-courses-3" class="rs-courses-3 sec-spacer">
        <div class="container">
            <div class="abt-title">
                <h2>دوراتنا</h2>
            </div>
            <!-- Courses Filter Tabs -->
            <div class="gridFilter">
                <button class="active" data-filter="*">الكل</button>
                <button data-filter=".{{ str_slug('برامج الدبلوم الإحترافي') }}">برامج الدبلوم الإحترافي</button>
                <button data-filter=".{{ str_slug('الدورات التدريبية') }}">الدورات التدريبية</button>
            </div>
            <!-- Courses Filter Tabs End -->

            <!-- Courses Blocks -->
            <div class="row grid">
                @foreach ($courses as $course)
                <div class="col-lg-4 col-md-6 grid-item {{ str_slug($course->category) }}">
                    <div class="course-item">
                        <div class="course-img">
                            <img src="{{ $course->image_url }}" alt="{{ $course->title }}" title="{{ $course->title }}" />
                            <div class="course-toolbar">
                                <h4 class="course-category"><a href="#">{{ $course->category }}</a></h4>
                            </div>
                        </div>
                        <div class="course-body">
                            <div class="course-desc">
                                <h4 class="course-title">
                                    <a href="{{ route('courses.show', [$course->id]) }}" title="{{ $course->title }}">
                                        {{ str_limit($course->title,50) }}
                                    </a>
                                </h4>
                                <div class="course-description">
                                    {!! $course->excerpt(64) !!}
                                </div>
                            </div>
                        </div>
                        <div class="course-footer">
                            <div class="course-seats">
                                <i class="fa fa-info-circle"></i> تنعقد: {{ $course->status }}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Courses Block End -->

            <!-- Courses Pagination -->
            {{ $courses->links() }}
            <!-- Courses Pagination -->
        </div>
    </div>
    <!-- Courses End -->
</div>
@endsection
