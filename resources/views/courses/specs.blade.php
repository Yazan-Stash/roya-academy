<div class="course-features-info">
    <h4 class="desc-title">ميّزات الدورة</h4>
    <ul>
        <li><i class="fa fa-files-o"></i>
            <span class="label">الفئة</span>
            <span class="value">{{ $course->category }}</span>
        </li>
        @isset($course->price)
        <li><i class="fa fa-dollar"></i>
            <span class="label">التكلفة</span>
            <span class="value">{{ $course->price }} JD</span>
        </li>
        @endisset
        @isset($course->duration)
        <li><i class="fa fa-clock-o"></i>
            <span class="label">المدّة</span>
            <span class="value">{{ $course->duration }}</span>
        </li>
        @endisset
        @isset($course->date)
        <li><i class="fa fa-calendar"></i>
            <span class="label">التاريخ</span>
            <span class="value">{{ $course->date }}</span>
        </li>
        @endisset
        @isset($course->seats)
        <li><i class="fa fa-users"></i>
            <span class="label">المقاعد</span>
            <span class="value">{{ $course->seats }} مقعد</span>
        </li>
        @endisset
        <li><i class="fa fa-info-circle"></i>
            <span class="label">موعد الدورة</span>
            <span class="value">{{ $course->status }}</span>
        </li>
    </ul>
</div>