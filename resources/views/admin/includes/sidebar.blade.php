<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-right image">
                <img src="/bower_components/admin-lte/dist/img/noprofile.jpeg" class="img-circle" alt="User Image">
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">القائمة الرئيسية</li>
            <li><a href="{{ route('admin.posts.index') }}"><i class="fa fa-newspaper-o"></i> الأخبار والفعاليّات</a></li>
            <li><a href="{{ route('admin.courses.index') }}"><i class="fa fa-book"></i> الدورات</a></li>
            <li><a href="{{ route('admin.staff.index') }}"><i class="fa fa-users"></i> كادر الأكاديمية</a></li>
            <li><a href="{{ route('admin.sliders.index') }}"><i class="fa fa-sliders"></i> الصور الرئيسيّة</a></li>
            <li><a href="{{ route('admin.gallery.index') }}"><i class="fa fa-file-image-o"></i> معرض الصور والفيديوهات </a></li>
            <li><a href="{{ route('admin.guests.index') }}"><i class="fa fa-group"></i> ضيوف الدورات </a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>


