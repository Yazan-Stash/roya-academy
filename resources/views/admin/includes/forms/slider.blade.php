{{ csrf_field() }}

<div id="form">
    <div class="row">
        <div class="col-xs-6">
            <div class="form-group">
                <label class="control-label">عنوان الصورة </label>
                <input name="heading" id="heading" class="form-control" type="text" value="{{ $slider->heading ?? old('heading', '') }}" placeholder="قم بإدخال عنوان الصورة">
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group">
                <label class="control-label">وصف الصورة </label>
                <textarea name="caption" id="caption" class="form-control" rows="4" placeholder="قم بإدخال وصف الصورة">{{ $slider->caption ?? old('caption', '') }}</textarea>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label">رابط الصورة</label>
                <input type="text" name="link" id="link" class="form-control" placeholder="الرابط الخارجي للصورة" value="{{ $slider->link ?? old('link', '') }}">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label"><span style="color:red">*</span> الصورة الرئيسية </label>
                <p>ملاحظة: يفضل استخدام الصور من صيغة PNG، حيث انها تسرع من اداء الموقع</p>
                <div class="main_image">
                    @isset($slider)
                    <p style="color: #880000; font-weight: bold;">سوف يتم حذف الصورة القديمة في حال رفع جديدة فقط</p>
                    <img style="width: 364px; margin-bottom: 10px" class="selected_img" src="{{ $slider->image_url }}"/>
                    @endisset
                    @include ('admin.includes.cropper-fields')
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <input type="submit" class="btn btn-primary btn-md" value="{{ $button }}">
                <a href="javascript:history.back(-1)" class="btn btn-default btn-md">إلغاء</a>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    @parent
    <script>
        /** TinyMCE **/
        tinymce.init({
            selector: 'textarea',
            height: 120,
            theme: 'modern',
            menubar: false,
            directionality: "rtl",
            fontsize: "18",
            paste_as_text: true,
            plugins: [
                'lists link code paste'
            ],
            toolbar: 'link | formatselect | bold italic | ' +
            'alignleft aligncenter alignright alignjustify | bullist numlist | ' +
            'removeformat | code ',
            content_css: [
                '/js/tinymce/tinymce_content.css'
            ]
        });
    </script>
@endsection

@include ('admin.includes.cropper')
