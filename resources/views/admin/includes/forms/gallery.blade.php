@section('style')
    <style>
        .video_row{
            display:flex;
            align-items:center;
        }
    </style>
@endsection

{{ csrf_field() }}

<div id="form">
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label"><span style="color:red">*</span> العنوان </label>
                <input name="title" id="title" class="form-control" type="text" value="{{ $gallery->title ?? old('title', '') }}" placeholder="قم بإدخال عنوان الصورة">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label" v-if="! edit"> نوع العنصر </label>
                <div class="radio" v-if=" ! edit">
                    <label>
                        <input type="radio" v-model="type" name="type" value="youtube">
                        Youtube
                    </label>
                </div>
                <div class="radio" v-if=" ! edit">
                    <label>
                        <input type="radio" v-model="type" name="type" value="video">
                         فيديو
                    </label>
                </div>
                <div class="radio" v-if=" ! edit">
                    <label>
                        <input type="radio" v-model="type" name="type" value="image">
                        صورة
                    </label>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row video_row">
        <div v-show="type == 'image'" class="col-xs-12">
            <div class="form-group">
                <label class="control-label"> الصورة </label>
                <p>ملاحظة: يفضل استخدام الصور من صيغة PNG، حيث انها تسرع من اداء الموقع</p>
                <div class="main_image">
                    @include ('admin.includes.cropper-fields')
                </div>
            </div>
        </div>

        <div v-show="type == 'video'" class="col-xs-12">
            <div class="form-group">
                <label class="control-label"> الفيديو </label>
                <div class="main_image">
                    <input type="file" name="video" />
                </div>
            </div>
        </div>

        <div v-show="type == 'youtube' && (isYoutube || ! edit)" class="col-xs-12">
            <div class="form-group">
                <label class="control-label"> رابط فيديو يوتيوب </label>
                <input name="youtube_url" id="youtube_url" class="form-control" type="text" value="{{ $gallery->youtube_url ?? old('youtube_url', '') }}" placeholder="https://www.youtube.com/embed/Xn8UJ8SwSTU">
            </div>
        </div>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary btn-md" value="{{ $button }}">
        <a href="javascript:history.back(-1)" class="btn btn-default btn-md">إلغاء</a>
    </div>
</div>

@include ('admin.includes.cropper')

@section ('scripts')
    @parent

    <script>
        vue = new Vue({
            el: '#form',
            data: {
                type: 'youtube',
                edit: {{ empty($gallery) ? 'false' : 'true' }},
                isYoutube: "{{ empty($gallery) ?  '' : $gallery->isYoutube() }}"
            }
        });
    </script>
@endsection
