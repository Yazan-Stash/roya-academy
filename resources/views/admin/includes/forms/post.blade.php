
{{ csrf_field() }}

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"><span style="color:red">*</span> عنوان الخبر | الفعاليّة </label>
            <input name="title" id="title" class="form-control" type="text" value="{{ $post->title ?? old('title', '') }}" placeholder="قم بإدخال عنوان الفعاليّة">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"><span style="color:red">*</span> محتوى الخبر | الفعاليّة </label>
            <textarea name="body" id="body" class="form-control" rows="4" placeholder="قم بإدخال المحتوى">{{ $post->body ?? old('body', '') }}</textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"><span style="color:red">*</span> صورة الخبر | الفعاليّة</label>
            <p>ملاحظة: يفضل استخدام الصور من صيغة PNG، حيث انها تسرع من اداء الموقع</p>
            <div class="main_image">
            @isset ($post)
                <p>سوف يتم حذف الصورة القديمة في حال رفع جديدة فقط</p>
                <img src="{{ $post->image_url }}" class="img-responsive" style="margin-bottom: 10px; max-width: 500px">
            @endisset
            
            @include ('admin.includes.cropper-fields')

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"><span style="color:red">*</span> حالة الخبر | الفعاليّة </label>
            <select name="published" class="form-control">
                <option value="1" {{ isset($post) ? ($post->published == 1 ? 'selected' : '') : (old('published') == '1' ? 'selected' : '') }}>منشور</option>
                <option value="0" {{ isset($post) ? ($post->published == 0 ? 'selected' : '') : (old('published') == '0' ? 'selected' : '') }}>غير منشور</option>
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <input type="submit" class="btn btn-primary btn-md" value="{{ $button }}">
    <a href="javascript:history.back(-1)" class="btn btn-default btn-md">إلغاء</a>
</div>

@section('scripts')
    @parent
    <script>
        /** Tags **/
        $('#tags').select2({
            dir:'rtl',
            tags:true
        });
    </script>
@endsection

@include ('admin.includes.cropper')
@include ('admin.includes.tinymce')
