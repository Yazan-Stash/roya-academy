{{ csrf_field() }}

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"> عنوان الدورة <span style="color:red">*</span></label>
            <input name="title" id="title" class="form-control" type="text" value="{{ $course->title ?? old('title', '') }}" placeholder="قم بإدخال عنوان الدورة">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-3">
        <div class="form-group">
            <label class="control-label"> الفئة <span style="color:red">*</span></label>
            <select name="category" class="form-control">
                <option value="برامج الدبلوم الإحترافي" {{ isset($course) ? ($course->category == 'برامج الدبلوم الإحترافي' ? 'selected' : '') : (old('category') == 'برامج الدبلوم الإحترافي' ? 'selected' : '') }}>برامج الدبلوم الإحترافي</option>
                <option value="الدورات التدريبية" {{ isset($course) ? ($course->category == 'الدورات التدريبية' ? 'selected' : '') : (old('category') == 'الدورات التدريبية' ? 'selected' : '') }}>الدورات التدريبية</option>
            </select>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="form-group">
            <label class="control-label"> التكلفة </label>
            <input name="price" id="price" class="form-control" type="text" value="{{ $course->price ?? old('price', '') }}" placeholder="قم بإدخال تكلفة الدورة">
        </div>
    </div>
    <div class="col-xs-3">
        <div class="form-group">
            <label class="control-label"> مُدّة الدورة </label>
            <input name="duration" id="duration" class="form-control" type="text" value="{{ $course->duration ?? old('duration', '') }}" placeholder="أسبوعان">
        </div>
    </div>
    <div class="col-xs-3">
        <div class="form-group">
            <label class="control-label"> عدد المقاعد </label>
            <input name="seats" id="seats" class="form-control" type="text" value="{{ $course->seats ?? old('seats', '') }}" placeholder="قم بإدخال عدد المقاعد المتوفرة">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-4">
        <div class="form-group">
            <label class="control-label"> تاريخ الدورة </label>
            <input name="date" id="date" class="form-control" type="text" value="{{ $course->date ?? old('date','') }}" placeholder="19-20-21-22 تموز (يوليو) 2018">
        </div>
    </div>
    <div class="col-xs-4">
        <div class="form-group">
            <label class="control-label"> المُحاضر/ين </label>
            <select name="instructors[]" id="instructors" class="form-control" multiple="multiple" data-placeholder="اختر المُحاضر/ين">
                @foreach($instructors as $instructor)
                    <option
                            value="{{ $instructor->id }}"
                            {{ in_array($instructor->id, $original_instructors ?? []) || in_array($instructor->id, old('instructors', [])) ? 'selected' : '' }}
                    >
                        {{ $instructor->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="form-group">
            <label class="control-label"> الضيوف </label>
            <select name="guests[]" id="guests" class="form-control" multiple="multiple" data-placeholder="اختر الضيوف">
                @foreach($guests as $guest)
                    <option
                            value="{{ $guest->id }}"
                            {{ in_array($guest->id, $original_guests ?? []) || in_array($guest->id, old('guests', [])) ? 'selected' : '' }}
                    >
                        {{ $guest->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> صورة الدورة (16:9) <span style="color:red">*</span></label>
            <p>ملاحظة: يفضل استخدام الصور من صيغة PNG، حيث انها تسرع من اداء الموقع</p>
            <div class="main_image">
            @isset ($course)
                <p>سوف يتم حذف الصورة القديمة في حال رفع جديدة فقط</p>
                <img src="{{ $course->image_url }}" class="img-responsive" style="margin-bottom: 10px;">
            @endisset
                
            @include ('admin.includes.cropper-fields')
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> موعد الدورة </label>
            <select name="status" class="form-control">
                <option value="حالياً" {{ isset($course) ? ($course->status == 'حالياً '? 'selected' : '') : (old('status') == 'حالياً' ? 'selected' : '') }}>حالياً</option>
                <option value="قريباً" {{ isset($course) ? ($course->status == 'قريباً' ? 'selected' : '') : (old('status') == 'قريباً' ? 'selected' : '') }}>قريباً</option>
                <option value="انتهت" {{ isset($course) ? ($course->status == 'انتهت' ? 'selected' : '') : (old('status') == 'انتهت' ? 'selected' : '') }}>انتهت</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"> محتوى الدورة <span style="color:red">*</span></label>
            <textarea name="description" id="description" class="form-control" rows="4" placeholder="قم بإدخال المحتوى">{{ $course->description ?? old('description', '') }}</textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <input type="submit" class="btn btn-primary btn-md" value="{{ $button }}">
    <a href="javascript:history.back(-1)" class="btn btn-default btn-md">إلغاء</a>
</div>

@section('scripts')
    @parent
    <script>
        /** Instructors **/
        $('#instructors').select2({
            dir:'rtl'
        });

        /** Instructors **/
        $('#guests').select2({
            dir:'rtl'
        });

        /** TinyMCE **/
        tinymce.init({
            selector: 'textarea',
            height: 400,
            theme: 'modern',
            menubar: false,
            directionality: "rtl",
            fontsize: "18",
            paste_as_text: true,
            media_dimensions:false,
            file_browser_callback_types: 'image',
            plugins: [
                'lists link code paste table media image'
            ],
            toolbar: 'image | media | link | formatselect | bold italic | ' +
            'alignleft aligncenter alignright alignjustify | bullist numlist | ' +
            'table | removeformat | code ',
            content_css: [
                '/js/tinymce/tinymce_content.css'
            ],
            file_browser_callback: function (field_name, url, type, win) {
                var w = window,
                    d = document,
                    e = d.documentElement,
                    g = d.getElementsByTagName('body')[0],
                    x = w.innerWidth || e.clientWidth || g.clientWidth,
                    y = w.innerHeight || e.clientHeight || g.clientHeight;

                var cmsURL = '/RichFilemanager/index.html?&field_name=' + field_name;

                if (type == 'image') {
                    cmsURL = cmsURL + "&type=images";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        });
    </script>
@endsection

@include ('admin.includes.cropper')
