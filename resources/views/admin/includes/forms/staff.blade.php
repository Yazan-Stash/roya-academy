
{{ csrf_field() }}

<div class="row">
    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> اسم المُحاضر <span style="color:red">*</span></label>
            <input name="name" id="name" class="form-control" type="text" placeholder="قم بإدخال اسم المُحاضر" value="{{ $staff->name ?? old('name', '') }}">
        </div>
    </div>

    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> الوظيفة / اللقب <span style="color:red">*</span></label>
            <input name="title" id="title" class="form-control" type="text" placeholder="قم بإدخال اسم المُحاضر" value="{{ $staff->title ?? old('title', '') }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> البريد الإلكتروني </label>
            <input name="email" id="email" class="form-control" type="text" placeholder="قم بإدخال البريد الإلكتروني" value="{{ $staff->email ?? old('email', '') }}">
        </div>
    </div>

    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> رقم الهاتف </label>
            <input name="phone" id="phone" class="form-control" type="text" placeholder="قم بإدخال رقم الهاتف" value="{{ $staff->phone ?? old('phone', '') }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> نبذة عن المُحاضر <span style="color:red">*</span></label>
            <textarea name="bio" id="bio" class="form-control" rows="4" placeholder="قم بإدخال اسم المُحاضر">{{ $staff->bio ?? old('bio', '') }}</textarea>
        </div>
    </div>

    <div class="col-xs-6">
        <div class="form-group">
            <label class="control-label"> صورة  المحُاضر (1:1) <span style="color:red">*</span></label>
            <p>ملاحظة: يفضل استخدام الصور من صيغة PNG، حيث انها تسرع من اداء الموقع</p>
            <div class="main_image">
            @isset ($staff)
                <p>سوف يتم حذف الصورة القديمة في حال رفع جديدة فقط</p>
                <img src="{{ $staff->image_url }}" class="img-responsive" style="margin-bottom: 10px">
            @endisset
                @include ('admin.includes.cropper-fields')
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <input type="submit" class="btn btn-primary btn-md" value="{{ $button }}">
    <a href="javascript:history.back(-1)" class="btn btn-default btn-md">إلغاء</a>
</div>

@include ('admin.includes.cropper', ['ratio' => '720 / 720'])
@include ('admin.includes.tinymce')
