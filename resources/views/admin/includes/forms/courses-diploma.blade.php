{{ csrf_field() }}

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"> برامج الدبلوم </label>
            <textarea name="diploma" id="diploma" class="form-control">{{ !empty($diploma) ? $diploma->diploma : '' }}</textarea>
        </div>
    </div>
</div>

<div class="form-group">
    <input type="submit" class="btn btn-primary btn-md" value="{{ $button }}">
    <a href="javascript:history.back(-1)" class="btn btn-default btn-md">إلغاء</a>
</div>

@include ('admin.includes.tinymce')
