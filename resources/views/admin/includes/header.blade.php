<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>R</b>A</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>أكاديمية</b> رؤيا</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- Main Action Links -->
        <ul class="nav navbar-nav">
            <li><a href="{{ route('admin.index') }}"><i class="fa fa-home"></i> الصفحة الرئيسية</a></li>
            <li><a href="{{ route('homepage') }}" target="_blank"><i class="fa fa-desktop"></i> مشاهدة الموقع</a></li>
            <li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out"></i> تسجيل الخروج</a></li>
        </ul>
    </nav>
</header>

