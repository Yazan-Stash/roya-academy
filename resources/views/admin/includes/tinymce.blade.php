@section ('scripts')
    @parent
    <script>
        /** TinyMCE **/
        tinymce.init({
            selector: 'textarea',
            height: 400,
            theme: 'modern',
            menubar: false,
            directionality: "rtl",
            fontsize: "18",
            paste_as_text: true,
            media_dimensions:false,
            plugins: [
                'lists link code paste table media'
            ],
            toolbar: 'media | link | formatselect | bold italic | ' +
            'alignleft aligncenter alignright alignjustify | bullist numlist | ' +
            'table | removeformat | code ',
            content_css: [
                '/js/tinymce/tinymce_content.css'
            ]
        });
    </script>
@endsection