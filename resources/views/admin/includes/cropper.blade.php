
@section ('scripts')
    @parent
    <script type="text/javascript" src="/js/cropper.js"></script>

    <script>
        /** Cropper **/
        $(document).ready(function () {
            $('#confirm_crop').click(function (e) {

                $(".image_cropper").cropper('getCroppedCanvas').toBlob(function (blob) {
                    var formData = new FormData();

                    formData.append('file', blob);

                    $.ajax('/api/upload-image/path', {
                        method: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('.image-path').val(data);
                            $(".cropper_done").fadeIn();
                            $("#confirm_crop").fadeOut();
                            $(".image_cropper").cropper('destroy');
                            $(".image_cropper").fadeOut();
                            e.preventDefault();
                        },
                        error: function () {
                            alert('Failed to upload image');
                        }
                    });
                });
            });

            $('#main_news_image').change(function () {
                $(".cropper_done_text").fadeOut();
                $("#confirm_crop").fadeIn();
                var file_data = $("#main_news_image").prop("files")[0];
                var form_data = new FormData();
                form_data.append("file", file_data);

                $.ajax({
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    url: '/api/upload-image/url'

                }).done(function (data) {
                    $(".image_cropper").attr("src", data);
                    var $image = $(".image_cropper");
                    $(".image_cropper").fadeIn();
                    originalData = {};

                    $image.cropper('destroy');

                    $image.cropper({
                        aspectRatio: {{ $ratio ?? '1280 / 720' }},
                        resizable: true,
                        zoomable: false,
                        rotatable: false,
                        multiple: true
                    });
                    $(".watermark").fadeIn();
                }).fail(function (jqXHR, status, errorThrown) {
                    console.log(errorThrown);
                    console.log(jqXHR.responseText);
                    console.log(jqXHR.status);
                });
            });
        });
    </script>
@endsection
