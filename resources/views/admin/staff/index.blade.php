@extends('admin.includes.master')
@section('title', 'كادر الأكاديمية')
@section('header', 'كادر الأكاديمية')

@section('content')
    <a href="{{ route('admin.staff.create') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة مُحاضر</button>
    </a>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة كادر الأكاديمية</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>الإسم</th>
                    <th>الصورة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($staff as $staff_member)
                    <tr>
                        <td class="ta_c">{{ $staff_member->id }}</td>
                        <td>{{ $staff_member->name }}</td>
                        <td class="ta_c">
                            <img src="{{ $staff_member->image_url }}" alt="{{ $staff_member->title }}" width="100" height="100" />
                        </td>
                        <td class="ta_c">
                            <a href="{{ route('admin.staff.edit',['id' => $staff_member->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.staff.delete',['id' => $staff_member->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>                 
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $staff->links() }}
        </div>
    </div>
@endsection
