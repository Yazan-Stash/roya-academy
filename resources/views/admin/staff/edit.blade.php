@extends('admin.includes.master')
@section('title', 'كادر الأكاديمية')
@section('header', 'كادر الأكاديمية')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">تعديل مُحاضر</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.staff.update',['id' => $staff->id]) }}" method="post" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @include('admin.includes.forms.staff', ['button' => 'تعديل'])
            </form>
        </div>
    </div>
@endsection