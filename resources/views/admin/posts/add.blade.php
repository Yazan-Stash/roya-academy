@extends('admin.includes.master')
@section('title', 'الأخبار والفعاليّات')
@section('header', 'الأخبار والفعاليّات')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">إضافة فعاليّة جديدة</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.posts.store') }}" method="post" enctype="multipart/form-data">
                @include('admin.includes.forms.post', ['button' => 'إضافة'])
            </form>
        </div>
    </div>
@endsection