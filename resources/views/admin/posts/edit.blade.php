@extends('admin.includes.master')
@section('title', 'الأخبار والفعاليّات')
@section('header', 'الأخبار والفعاليّات')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">تعديل فعاليّة</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.posts.update',['id' => $post->id]) }}" method="post" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @include('admin.includes.forms.post', ['button' => 'تعديل'])
            </form>
        </div>
    </div>
@endsection