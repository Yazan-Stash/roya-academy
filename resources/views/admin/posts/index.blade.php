@extends('admin.includes.master')
@section('title', 'الأخبار والفعاليّات')
@section('header', 'الأخبار والفعاليّات')

@section('content')
    <a href="{{ route('admin.posts.create') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة فعاليّة</button>
    </a>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة الأخبار والفعاليّات</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>العنوان</th>
                    <th>تاريخ النشر</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td class="ta_c">{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td class="ta_c">{{ $post->formatted_created_at }}</td>
                        <td class="ta_c">
                            <a href="{{ route('admin.posts.edit',['id' => $post->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.posts.delete',['id' => $post->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $posts->links() }}
        </div>
    </div>
@endsection
