@extends('admin.includes.master')
@section('title', 'عن برامج الدبلوم الإحترافي')
@section('header', 'عن برامج الدبلوم الإحترافي')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">تعديل وصف برامج الدبلوم الإحترافي</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.courses.diploma.update') }}" method="post" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @include('admin.includes.forms.courses-diploma', ['button' => 'تعديل'])
            </form>
        </div>
    </div>
@endsection
