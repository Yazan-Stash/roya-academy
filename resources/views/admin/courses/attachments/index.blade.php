@extends('admin.includes.master')
@section('title', 'الملفّات المرفقة')
@section('header', 'الملفّات المرفقة')

@section('content')
    <a href="#" data-toggle="modal" data-target="#addAttachment">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة ملف PDF</button>
    </a>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة الملفّات المرفقة لدورة </h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>العنوان</th>
                    <th>تحميل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($attachments as $attachment)
                    <tr>
                        <td class="ta_c">{{ $attachment->id }}</td>
                        <td>{{ $attachment->title }}</td>
                        <td class="text-center">
                            <a href="{{ $attachment->download_url }}" title="{{ $attachment->title }}" download>تحميل</a>
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.courses.attachments.delete', ['course' => $course->id, 'attachment' => $attachment->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Add Attachment -->
    <div id="addAttachment" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form action="{{ route('admin.courses.attachments.store', ['course' => $course->id]) }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}
                
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">إضافة ملف مرفق PDF</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">عنوان الملف المرفق <span style="color:red">*</span></label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="قم بإدخال عنوان للملف المراد إرفاقه">
                        </div>
                        <div class="form-group">
                            <label class="control-label">الملف <span style="color:red">*</span> </label>
                            <input type="file" name="attachment" id="attachment">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">إضافة</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
