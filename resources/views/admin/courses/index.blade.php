@extends('admin.includes.master')
@section('title', 'الدورات')
@section('header', 'الدورات')

@section('content')
    <!-- Add New Course -->
    <a href="{{ route('admin.courses.create') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة دورة</button>
    </a>
    <!-- Add New Course End -->

    <!-- Course Introduction -->
    <a href="{{ route('admin.courses.intro.edit') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-edit"></i> عن الدورات</button>
    </a>
    <!-- Course Introduction End -->

    <!-- Course Diploma -->
    <a href="{{ route('admin.courses.diploma.edit') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-edit"></i> عن برامج الدبلوم الإحترافي</button>
    </a>
    <!-- Course Diploma End -->

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة الدورات</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>العنوان</th>
                    <th>الفئة</th>
                    <th>الملفّات المرفقة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($courses as $course)
                    <tr>
                        <td class="ta_c">{{ $course->id }}</td>
                        <td>{{ $course->title }}</td>
                        <td class="ta_c">{{ $course->category }}</td>
                        <td class="ta_c">
                            <a href="{{ route('admin.courses.attachments.index',['id' => $course->id]) }}">
                                <i class="fa fa-file-pdf-o"></i>
                            </a>
                        </td>
                        <td class="ta_c">
                            <a href="{{ route('admin.courses.edit',['id' => $course->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.courses.delete',['id' => $course->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $courses->links() }}
        </div>
    </div>
@endsection