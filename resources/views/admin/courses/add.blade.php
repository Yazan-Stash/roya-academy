@extends('admin.includes.master')
@section('title', 'الدورات')
@section('header', 'الدورات')

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">إضافة دورة جديدة</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('admin.courses.store') }}" method="post" enctype="multipart/form-data">
                @include('admin.includes.forms.course', ['button' => 'إضافة'])
            </form>
        </div>
    </div>
@endsection