@extends('layouts.app')

@section('content')
<div class="hold-transition register-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>أكاديمية رؤيا</b> RA </a>
        </div>

        <div class="login-box-body">
            <p class="login-box-msg">تسجيل عضوية جديدة</p>

            <form method="POST" action="{{ route('admin.register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required placeholder="الإسم">
                    <span class="fa fa-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                           placeholder="البريد الإلكتروني">
                    <span class="fa fa-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    <input id="password" type="password" class="form-control" name="password" placeholder="كلمة المرور" required>
                    <span class="fa fa-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                    @endif
                </div>

                <div class="form-group has-feedback">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           placeholder="تأكيد كلمة المرور" required>
                    <span class="fa fa-sign-out form-control-feedback"></span>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">تسجيل</button>
                    </div>
                </div>
            </form>

            <div style="margin-top:10px">
                <a href="{{ route('admin.login') }}">لدي عضوية</a>
            </div>
        </div>
    </div>
</div>
@endsection
