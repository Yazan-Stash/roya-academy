@extends('layouts.app')

@section('content')
<div class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>أكاديمية رؤيا</b> RA </a>
        </div>

        <div class="login-box-body">
            <p class="login-box-msg">قم بتسجيل الدخول</p>

            <form method="POST" action="{{ route('admin.login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus
                           placeholder="البريد الإلكتروني">
                    <span class="fa fa-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    <input id="password" type="password" class="form-control" name="password" placeholder="كلمة المرور" required>
                    <span class="fa fa-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="row">
                    <div class="col-xs-5">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">تسجيل دخول</button>
                    </div>
                    <div class="col-xs-7">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>  تذكرني
                            </label>
                        </div>
                    </div>
                </div>
            </form>

            <a href="{{ route('admin.password.request') }}"> نسيت كلمة المرور </a><br>
            <a href="{{ route('admin.register') }}" class="text-center"> تسجيل عضوية جديدة </a>
        </div>
    </div>
</div>
@endsection
