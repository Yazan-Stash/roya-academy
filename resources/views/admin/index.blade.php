@extends('admin.includes.master')
@section('title', 'الصفحة الرئيسية')

@section('content')
    <h4>مرحباً بك بإدارة موقع أكاديمية رؤيا للتدريب الإعلامي</h4>

    <div class="row">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">لائحة الدورات الإعلاميّة</h3>
                    <div class="box-tools pull-left">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th>الرقم</th>
                            <th>عنوان الدورة</th>
                            <th>تعديل</th>
                            <th>حذف</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($courses as $course)
                            <tr>
                                <td class="ta_c">{{ $course->id }}</td>
                                <td>{{ $course->title }}</td>
                                <td class="ta_c">
                                    <a href="{{ route('admin.courses.edit',['id' => $course->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <td class="ta_c">
                                    <form action="{{ route('admin.courses.delete',['id' => $course->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">لائحة النشاطات والفعاليّات</h3>
                    <div class="box-tools pull-left">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                        <tr>
                            <th>الرقم</th>
                            <th>الإسم</th>
                            <th>تعديل</th>
                            <th>حذف</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td class="ta_c">{{ $post->id }}</td>
                                <td>{{ $post->title }}</td>
                                <td class="ta_c">
                                    <a href="{{ route('admin.posts.edit',['id' => $post->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <td class="ta_c">
                                    <form action="{{ route('admin.posts.delete',['id' => $post->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection