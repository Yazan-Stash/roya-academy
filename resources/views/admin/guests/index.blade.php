@extends('admin.includes.master')
@section('title', 'الضُيوف')
@section('header', 'الضُيوف')

@section('content')
    <a href="{{ route('admin.guests.create') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة ضيف</button>
    </a>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة الضُيوف</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>الإسم</th>
                    <th>الصورة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($guests as $guest)
                    <tr>
                        <td class="ta_c">{{ $guest->id }}</td>
                        <td>{{ $guest->name }}</td>
                        <td class="ta_c">
                            <img src="{{ $guest->image_url }}" alt="{{ $guest->name }}" width="100" height="100" />
                        </td>
                        <td class="ta_c">
                            <a href="{{ route('admin.guests.edit',['id' => $guest->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.guests.delete',['id' => $guest->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>                 
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $guests->links() }}
        </div>
    </div>
@endsection
