@extends('admin.includes.master')
@section('title', 'الصور الرئيسية')
@section('header', 'الصور الرئيسية')

@section('content')
    <a href="{{ route('admin.sliders.create') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة صورة</button>
    </a>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة الصور الرئيسية</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>العنوان</th>
                    <th>الصورة</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($slider as $slider_item)
                    <tr>
                        <td class="ta_c">{{ $slider_item->id }}</td>
                        <td>{{ $slider_item->heading }}</td>
                        <td class="ta_c">
                            <img src="{{ $slider_item->image_url }}" alt="{{ $slider_item->heading }}" width="200" height="120" />
                        </td>
                        <td class="ta_c">
                            <a href="{{ route('admin.sliders.edit',['id' => $slider_item->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.sliders.delete',['id' => $slider_item->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $slider->links() }}
        </div>
    </div>
@endsection
