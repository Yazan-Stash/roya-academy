@extends('admin.includes.master')
@section('title', 'معرض الصور والفيديوهات')
@section('header', 'معرض الصور والفيديوهات')

@section('content')
    <a href="{{ route('admin.gallery.create') }}">
        <button class="btn btn-default btn-md mb_10"><i class="fa fa-plus-square"></i> إضافة صورة أو فيديو</button>
    </a>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">لائحة معرض الصور والفيديوهات</h3>
            <div class="box-tools pull-left">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>

        <div class="box-body">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>الرقم</th>
                    <th>العنوان</th>
                    <th>النوع</th>
                    <th>تعديل</th>
                    <th>حذف</th>
                </tr>
                </thead>

                <tbody>
                @foreach($gallery as $gallery_image)
                    <tr>
                        <td class="ta_c">{{ $gallery_image->id }}</td>
                        <td>{{ $gallery_image->title }}</td>
                        <td class="ta_c">
                            {{ $gallery_image->type }}
                        </td>
                        <td class="ta_c">
                            @if ($gallery_image->isYoutube())
                                <a href="{{ route('admin.gallery.edit',['id' => $gallery_image->id]) }}">                           
                                    <i class="fa fa-edit"></i>
                                </a>
                            @else
                                <i class="fa fa-minus"></i>    
                            @endif
                        </td>
                        <td class="ta_c">
                            <form action="{{ route('admin.gallery.delete',['id' => $gallery_image->id]) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $gallery->links() }}
        </div>
    </div>
@endsection
